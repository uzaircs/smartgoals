import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Task, Goal } from '../service/database.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GlobalStorageService } from '../service/global-storage.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.page.html',
  styleUrls: ['./task-view.page.scss', '../header.scss'],
})
export class TaskViewPage implements OnInit {
  port: any;
  task: Task;
  subcription: Subscription;
  ready: boolean;
  goal: Goal;
  constructor(
    public toastController: ToastController,
    private route: ActivatedRoute,
    private db: GlobalStorageService,
    private router: Router
  ) { }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Task saved successfully.',
      duration: 2000,
      color: 'success',
      header: 'Saved',
      translucent: true
    });
    toast.present();
  }
  defferedLoad(hash: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      let task = null;
      this.subcription = this.db.getTasks().subscribe(tasks => {
        task = tasks.filter(t => t.hash === hash);
      });
      setTimeout(() => {
        if (task && task[0]) {
          this.task = task[0];
          this.db.getGoal(this.task.goal).then(g => {
            this.goal = g;
          });
        } else {
          reject('Task Not Found');
        }
        resolve(true);
      }, 100);
    });
  }
  save() {
    this.db.updateTask(this.task.hash, this.task).then(done => {
      this.router.navigate(['/goal-view/', this.task.goal]).then(done => {
        this.presentToast();
      });
    });

  }
  ionViewDidLeave() {
    if (this.subcription) {
      this.subcription.unsubscribe();
    }
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      const hash = params.hash;

      if (hash) {
        this.defferedLoad(hash).then(ready => {
          this.ready = true;
        });
      }
    });
  }

}
