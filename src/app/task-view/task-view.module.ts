import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskViewPageRoutingModule } from './task-view-routing.module';

import { TaskViewPage } from './task-view.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicSelectableModule,
    IonicModule,
    TaskViewPageRoutingModule
  ],
  declarations: [TaskViewPage]
})
export class TaskViewPageModule {}
