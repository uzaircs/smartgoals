import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { UIComponentsModule } from '../uicomponents/uicomponents.module';
import { DingSoundService } from '../utils/ding-sound.service';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    UIComponentsModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [Tab1Page],
  providers: [DingSoundService]
})
export class Tab1PageModule { }
