import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DingSoundService } from '../utils/ding-sound.service';
import { Goal } from '../service/database.service';
import { GlobalStorageService } from '../service/global-storage.service';
import { Indicator, HabitIndicator, IndicatorStatus } from '../interfaces/indicator';
import { PushService } from '../service/Notifications/push.service';
import { HabitItem, HabitHistory, HistoryItem } from '../models/habits/habit-item';
import { ValidatorService } from '../pages/habit-wizard/habit-container/validator.service';
import { RepeatItem, TimePeriod } from '../models/repeat/repeat-item';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss', '../header.scss'],

})
export class Tab1Page {
  ready = false;
  overviewSlideOpts = {
    slidesPerView: 1.5
  };
  slideOpts = {
    effect: 'coverflow',
    centeredSlides: true,
    centeredSlidesBounds: true,
   /*  loop: true, */
    initialSlide: 1,
    slidesPerView: 1.5,
    coverflowEffect: {
      rotate: 10,
      stretch: 0,
      depth: 100,
      modifier: 1,
    },
    on: {
      beforeInit() {
        const swiper = this;

        swiper.classNames.push(`${swiper.params.containerModifierClass}coverflow`);
        swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);

        swiper.params.watchSlidesProgress = true;
        swiper.originalParams.watchSlidesProgress = true;
      },
      setTranslate() {
        const swiper = this;
        const {
          width: swiperWidth, height: swiperHeight, slides, $wrapperEl, slidesSizesGrid, $
        } = swiper;
        const params = swiper.params.coverflowEffect;
        const isHorizontal = swiper.isHorizontal();
        const transform$$1 = swiper.translate;
        const center = isHorizontal ? -transform$$1 + (swiperWidth / 2) : -transform$$1 + (swiperHeight / 2);
        const rotate = isHorizontal ? params.rotate : -params.rotate;
        const translate = params.depth;
        // Each slide offset from center
        for (let i = 0, length = slides.length; i < length; i += 1) {
          const $slideEl = slides.eq(i);
          const slideSize = slidesSizesGrid[i];
          const slideOffset = $slideEl[0].swiperSlideOffset;
          const offsetMultiplier = ((center - slideOffset - (slideSize / 2)) / slideSize) * params.modifier;

          let rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
          let rotateX = isHorizontal ? 0 : rotate * offsetMultiplier;
          // var rotateZ = 0
          let translateZ = -translate * Math.abs(offsetMultiplier);

          let translateY = isHorizontal ? 0 : params.stretch * (offsetMultiplier);
          let translateX = isHorizontal ? params.stretch * (offsetMultiplier) : 0;

          // Fix for ultra small values
          if (Math.abs(translateX) < 0.001) translateX = 0;
          if (Math.abs(translateY) < 0.001) translateY = 0;
          if (Math.abs(translateZ) < 0.001) translateZ = 0;
          if (Math.abs(rotateY) < 0.001) rotateY = 0;
          if (Math.abs(rotateX) < 0.001) rotateX = 0;

          const slideTransform = `translate3d(${translateX}px,${translateY}px,${translateZ}px)  rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;

          $slideEl.transform(slideTransform);
          $slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
          if (params.slideShadows) {
            // Set shadows
            let $shadowBeforeEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
            let $shadowAfterEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');
            if ($shadowBeforeEl.length === 0) {
              $shadowBeforeEl = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'left' : 'top'}"></div>`);
              $slideEl.append($shadowBeforeEl);
            }
            if ($shadowAfterEl.length === 0) {
              $shadowAfterEl = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'right' : 'bottom'}"></div>`);
              $slideEl.append($shadowAfterEl);
            }
            if ($shadowBeforeEl.length) $shadowBeforeEl[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
            if ($shadowAfterEl.length) $shadowAfterEl[0].style.opacity = (-offsetMultiplier) > 0 ? -offsetMultiplier : 0;
          }
        }

        // Set correct perspective for IE10
        if (swiper.support.pointerEvents || swiper.support.prefixedPointerEvents) {
          const ws = $wrapperEl[0].style;
          ws.perspectiveOrigin = `${center}px 50%`;
        }
      },
      setTransition(duration) {
        const swiper = this;
        swiper.slides
          .transition(duration)
          .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
          .transition(duration);
      }
    },
    breakpoints: {
      1500: {
        slidesPerView: 3.2,
        spaceBetween: 15,
      },
      1000: {
        slidesPerView: 2.3,
        spaceBetween: 15,
      },
      500: {
        slidesPerView: 1.4,
        spaceBetween: 0,
      }
    }

  };
  goals: Goal[] = [];
  habits: HabitItem[];
  constructor(
    private router: Router,
    private ding: DingSoundService,
    private db: GlobalStorageService,
    private notification: PushService,
    private validator: ValidatorService,
    private nativePageTransitions: NativePageTransitions
  ) {
    this.deferredLoad().then(ready => {
      this.ready = ready;
    });
    notification.send({
      message: 'Your task is overdue!',
      title: 'Task Overdue',
    });
  }
  deferredLoad(): Promise<boolean> {
    return new Promise(resolve => {

      this.db.getGoals().subscribe(goals => {
        this.goals = goals;
        this.db.getHabits().subscribe(habits => {
          this.habits = [];
          this.habits = habits;
          resolve(true);
        });


      });
    });
  }

  viewGoal(goal: Goal) {
    this.router.navigate(['/goal-view/', goal.hash]);
  }
  ionViewWillLeave() {



  }

}
