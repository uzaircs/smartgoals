import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RevisionComponent } from './pages/habit/revision/revision.component';

const routes: Routes = [
  /*  {
     path: '',
     loadChildren: () => import('./pages/home/home.module').then(done => done.HomePageModule)
   }, */
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(done => done.TabsPageModule)
  },
  {
    path: 'goal-view/:goal',
    loadChildren: () => import('./goal-view/goal-view.module').then(m => m.GoalViewPageModule)
  },
  {
    path: 'task-view/:hash',
    loadChildren: () => import('./task-view/task-view.module').then(m => m.TaskViewPageModule)
  },
  {
    path: 'choice-form',
    loadChildren: () => import('./choice-form/choice-form.module').then(m => m.ChoiceFormPageModule)
  },
  {
    path: 'task-form/:goal',
    loadChildren: () => import('./task-form/task-form.module').then(m => m.TaskFormPageModule)
  },
  {
    path: 'goals-form',
    loadChildren: () => import('./goals-form/goals-form.module').then(m => m.GoalsFormPageModule)
  },

  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'task-preview/:hash',
    loadChildren: () => import('./pages/task-preview/task-preview.module').then(m => m.TaskPreviewPageModule)
  },
  {
    path: 'habit-wizard',
    loadChildren: () => import('./pages/habit-wizard/habit-wizard.module').then(m => m.HabitWizardPageModule)
  },
  {
    path: 'simple-count-down',
    loadChildren: () => import('./pages/timer/simple-count-down/simple-count-down.module').then(m => m.SimpleCountDownPageModule)
  },
  {
    path: 'goal-wizard',
    loadChildren: () => import('./pages/goals/goalwizard/goalwizard.module').then(m => m.GoalwizardPageModule)
  },
  {
    path: 'goal-edit/:hash',
    loadChildren: () => import('./pages/goals/goaledit/goaledit.module').then(m => m.GoaleditPageModule)
  },
  {
    path: 'habit/:hash',
    loadChildren: () => import('./pages/habit/habit.module').then(m => m.HabitPageModule)
  },
  {
    path: 'habit/edit/:hash',
    component: RevisionComponent
  },
  {
    path: 'calendar-view',
    loadChildren: () => import('./uicomponents/calendar/calendar-view/calendar-view.module').then(m => m.CalendarViewPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
