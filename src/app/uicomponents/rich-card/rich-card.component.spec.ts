import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RichCardComponent } from './rich-card.component';

describe('RichCardComponent', () => {
  let component: RichCardComponent;
  let fixture: ComponentFixture<RichCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RichCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RichCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
