import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Goal } from 'src/app/service/database.service';
import * as moment from 'moment';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { GoalStatistics } from 'src/app/interfaces/goal-statistics';
import { StatisticsService } from 'src/app/service/statistics.service';

@Component({
  selector: 'app-rich-card',
  templateUrl: './rich-card.component.html',
  styleUrls: ['./rich-card.component.scss'],

})
export class RichCardComponent implements OnInit {
  expired = false;
  @Input() goal: Goal;
  value: number;
  ready: boolean;
  goalStats: GoalStatistics;
  constructor(private db: GlobalStorageService, private statService: StatisticsService) {

  }
  getStats(goal: Goal): Promise<GoalStatistics> {
    return new Promise((resolve, reject) => {
      this.statService.goalStats(goal).then(stats => {

        resolve(stats);
      }).catch(err => reject(err));
    });
  }
  calculateValue(goal: Goal): Promise<number> {
    return new Promise((resolve, reject) => {
      this.db.getTasks().subscribe(tasks => {

        const goalTasks = tasks.filter(t => t.goal === goal.hash);
        if (goalTasks && goalTasks.length) {
          const count = goalTasks.length;

          const completedCount = goalTasks.filter(t => t.status === 2).length;
          let value = count;
          if (completedCount) {
            value = (completedCount / value) * 100;
            resolve(value);
          } else {
            resolve(0);
          }
        }

      });
    });
  }
  setValue() {
    this.calculateValue(this.goal).then(value => {
      this.value = value;
    });
  }
  checkExpired(): boolean {
    return (moment(this.goal.end_date).isBefore(moment()));
  }
  /*  ngOnChanges(changes: SimpleChanges): void {
     this.expired = this.checkExpired();
     this.setValue();
   } */
  deferredLoad(): Promise<boolean> {
    return new Promise(resolve => {
      this.db.getTasks().subscribe(tasks => {
        this.setValue();
        this.expired = this.checkExpired();
        this.getStats(this.goal).then(stats => {
          this.goalStats = stats;
        });
        setTimeout(() => {
          resolve(true);
        }, 700);
      });
    });
  }
  ngOnInit() {
    this.deferredLoad().then(ready => {
      this.ready = true;
    });


  }

}
