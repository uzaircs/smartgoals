import { Component, OnInit, Input } from '@angular/core';
import { Indicator, IndicatorStatus } from 'src/app/interfaces/indicator';

@Component({
  selector: 'app-history-item',
  templateUrl: './history-item.component.html',
  styleUrls: ['./history-item.component.scss'],
})
export class HistoryItemComponent implements OnInit {
  @Input() indicator: Indicator;
  IndicatorStatus: IndicatorStatus;
  constructor() { }

  ngOnInit() { }

}
