import { Component, OnInit } from '@angular/core';
import { Vibration } from '@ionic-native/vibration/ngx';
import { ActionSheetController } from '@ionic/angular';
@Component({
  selector: 'app-quick-task',
  templateUrl: './quick-task.component.html',
  styleUrls: ['./quick-task.component.scss'],
})
export class QuickTaskComponent implements OnInit {

  isPressed = false;
  isActive = false;
  isCompleted = false;

  constructor(private vibration: Vibration, private actionSheetController: ActionSheetController) { }
  pressed() {
    this.isActive = true;
    this.vibration.vibrate(200);
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Some Habit',
      buttons: [{
        text: 'Completed Once',
        icon: 'checkmark-circle',

        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Completed All',
        icon: 'done-all',
        handler: () => {
          console.log('Share clicked');
        }
      },
      {
        text: 'Discard One',
        icon: 'remove-circle-outline',
        handler: () => {
          console.log('Favorite clicked');
        }
      },
      {
        text: 'Reset',
        icon: 'refresh',
        handler: () => {
          console.log('Play clicked');
        }
      },
      {
        text: 'Edit',
        icon: 'create',
        handler: () => {
          console.log('Play clicked');
        }
      },

      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  tapped() {
    this.presentActionSheet();
  }
  released() {
    this.isActive = false;
  }

  active() {
    this.isCompleted = true;
    this.vibration.vibrate(600);
  }
  ngOnInit() { }

}
