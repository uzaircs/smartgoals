import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavController } from '@ionic/angular';
import { Task } from 'src/app/service/database.service';
import * as moment from 'moment';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
export class DelayItem {
  date: Date;
  text: string;
}
@Component({
  selector: 'app-delay-modal',
  templateUrl: './delay-modal.component.html',
  styleUrls: ['./delay-modal.component.scss'],
})

export class DelayModalComponent implements OnInit {
  options: DelayItem[] = [];
  task: Task;
  constructor(
    private modal: ModalController,
    private toast: ToastController,
    private nav: NavController,
    private db: GlobalStorageService
  ) { }
  dismiss() {

    this.modal.dismiss();
  }
  async noDueDateToast() {
    const toast = await this.toast.create({
      message: 'This task can not be delayed as it does not have a due date',
      header: 'Can not be delayed',
      duration: 3000,
      color: 'warning'
    });
    toast.present();
  }
  verifyDueDate() {
    if (!this.task.due_date) {
      this.noDueDateToast().then(done => {
        this.dismiss();
      });


    }
  }
  fill() {
    for (let i = 0; i < 10; i++) {
      const item = new DelayItem();
      item.date = moment(this.task.due_date).add(i + 1, 'hours').toDate();
      item.text = (i + 1) + ' Hours';
      this.options.push(item);
    }
    for (let i = 0; i < 10; i++) {
      const item = new DelayItem();
      item.date = moment(this.task.due_date).add(i + 1, 'days').toDate();
      item.text = (i + 1) + ' Days';
      this.options.push(item);
    }
  }
  addTime(date: Date, hour: number): Date {
    return moment(date).add(hour, 'hours').toDate();
  }
  ngOnInit() {
    this.fill();
    this.verifyDueDate();
  }
  async delayToast(text) {
    const toast = await this.toast.create({
      message: 'Task Delayed by ' + text,
      header: 'Delayed',
      color: 'tertiary',
      duration: 2000
    });
    toast.present();
  }
  delayTask(option: DelayItem) {
    this.task.due_date = option.date;
    this.db.updateTask(this.task.hash, this.task).then(done => {
      this.delayToast(option.text).then(completed => {
        this.dismiss();
      });
    });
  }
}
