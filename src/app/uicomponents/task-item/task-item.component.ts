import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DingSoundService } from 'src/app/utils/ding-sound.service';
import { Task } from 'src/app/service/database.service';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { ToastController, ActionSheetController, AlertController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { DelayModalComponent } from './delay-modal/delay-modal.component';
@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
})
export class TaskItemComponent implements OnInit {
  isCompleted = false;
  isAnimating = false;
  isOverdue = false;
  @Input() task: Task;
  constructor(
    private router: Router,
    private ding: DingSoundService,
    private db: GlobalStorageService,
    private toast: ToastController,
    private actionSheet: ActionSheetController,
    private alert: AlertController,
    private modal: ModalController
  ) { }
  async presentDelay() {
    const modal = await this.modal.create({
      component: DelayModalComponent,
      mode: 'ios',
      animated: true,
      componentProps: { task: this.task }
    });
    modal.present();
  }
  async alertDelete() {
    const alert = await this.alert.create({
      message: 'Are you sure you want to delete this task?',
      subHeader: 'you will loose all progress for this task',
      buttons: [
        {
          text: 'No, Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'desctructive',
          handler: () => {
            this.db.deleteTask(this.task.hash).then(done => {
              this.deleteToast();
            });

          }

        }
      ]

    });
    alert.present();
  }
  async presentToast() {
    const toast = await this.toast.create({
      message: 'Task status changed to Pending',
      duration: 1500,

    });
    toast.present();
  }
  async undoToast() {
    const toast = await this.toast.create({
      message: 'Task restored',
      duration: 1500,
      color: 'success'
    });
    toast.present();
  }
  undoDelete() {
    this.task.deleted = false;
    this.db.updateTask(this.task.hash, this.task).then(done => {
      this.undoToast();
    });
  }
  async deleteToast() {
    const toast = await this.toast.create({
      message: 'Task deleted succesfully',
      header: 'Task deleted',
      duration: 3000,
      color: 'primary',
      buttons: [{
        text: 'Undo',
        handler: () => {
          this.undoDelete();
        }
      }]
    });
    toast.present();
  }
  async taskActions(task: Task) {
    const sheet = await this.actionSheet.create({
      header: 'What would you like to do?',
      buttons: [
        {
          text: 'View Task',
          icon: 'document-text-outline',
          handler: () => {
            this.router.navigate(['/task-preview/', task.hash]);
          }
        },
        {
          text: 'Edit Task',
          icon: 'create-outline',
          handler: () => {
            this.router.navigate(['/task-view/', task.hash]);
          }
        },

        {
          text: 'Delay Task',
          icon: 'calendar-outline',
          handler: () => {
            this.presentDelay();
          }
        },
        {
          text: 'Delete Task',
          icon: 'trash-outline',
          role: 'destructive',
          handler: () => {
            this.alertDelete();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]

    });
    sheet.present();
  }
  /*  open_task() {
     this.router.navigate(['/task-view/', this.task.hash]);
   } */
  done() {
    this.task.status = 2;
    this.db.updateTask(this.task.hash, this.task).then(done => {
      this.ding.play();
      this.isCompleted = true;
      this.isAnimating = true;
      setTimeout(() => {
        this.isAnimating = false;
      }, 1500);
    });

  }
  delayActions() {

  }
  undone() {
    this.task.status = 0;
    this.db.updateTask(this.task.hash, this.task).then(done => {
      this.presentToast();
    });
    this.isCompleted = false;
  }
  isExpired(task: Task): boolean {
    return moment(task.due_date).isBefore(moment());
  }
  ngOnInit() {
    this.isOverdue = this.isExpired(this.task);

  }

}

