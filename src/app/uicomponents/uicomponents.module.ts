import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressComponent } from './progress/progress.component';
import { RichCardComponent } from './rich-card/rich-card.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { QuickTaskComponent } from './quick-task/quick-task.component';
import { SettingsItemComponent } from './settings-item/settings-item.component';
import { IonicModule } from '@ionic/angular';
import { DingSoundService } from '../utils/ding-sound.service';
import { FromNowPipe } from '../pipes/from-now.pipe';
import { PipesModule } from '../pipes/pipes/pipes.module';
import { HistoryItemComponent } from './history-item/history-item.component';
import { HistoryStripComponent } from './history-strip/history-strip.component';
import { HabitCardComponent } from './habit-card/habit-card.component';
import { GlobalStorageService } from '../service/global-storage.service';
import { MonthViewComponent } from './calendar/calendar-view/month-view/month-view.component';
import { CalendarViewPageModule } from './calendar/calendar-view/calendar-view.module';
import { DelayModalComponent } from './task-item/delay-modal/delay-modal.component';


@NgModule({
  declarations: [
    ProgressComponent,
    RichCardComponent,
    TaskItemComponent,
    QuickTaskComponent,
    SettingsItemComponent,
    HistoryItemComponent,
    HistoryStripComponent,
    HabitCardComponent,
    MonthViewComponent,
    DelayModalComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,

  ],
  providers: [DingSoundService, FromNowPipe],
  exports: [
    ProgressComponent,
    MonthViewComponent,
    RichCardComponent,
    TaskItemComponent,
    HabitCardComponent,
    HistoryItemComponent,
    HistoryStripComponent,
    QuickTaskComponent,
    SettingsItemComponent,
    DelayModalComponent]

})
export class UIComponentsModule { }
