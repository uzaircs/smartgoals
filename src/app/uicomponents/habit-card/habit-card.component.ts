import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActionSheetController, PopoverController, ToastController, NavController } from '@ionic/angular';
import { HabitActionsPopoverComponent } from 'src/app/popovers/habit-actions-popover/habit-actions-popover.component';
import { HabitItem, HabitHistory } from 'src/app/models/habits/habit-item';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { ValidatorService, CheckResultItem, HabitSummaryItem } from 'src/app/pages/habit-wizard/habit-container/validator.service';
import { DingSoundService } from 'src/app/utils/ding-sound.service';

@Component({
  selector: 'app-habit-card',
  templateUrl: './habit-card.component.html',
  styleUrls: ['./habit-card.component.scss'],
})
export class HabitCardComponent implements OnInit {
  @Input() habit: HabitItem;
  results: CheckResultItem[];
  relative: string;
  summary: HabitSummaryItem;
  constructor(
    private popover: PopoverController,
    private db: GlobalStorageService,
    private validator: ValidatorService,
    private ding: DingSoundService,
    private actionSheet: ActionSheetController,
    private toastr: ToastController,
    private nav: NavController
  ) {

  }
  async removedToast() {
    const toast = await this.toastr.create({
      message: 'One history discarded',
      color: 'primary',
      header: 'Discarded',
      duration: 2000

    });
    toast.present();
  }
  async presentRemoveOptions() {
    const sheet = await this.actionSheet.create({
      header: 'What would you like to do?',
      subHeader: 'Applies for ' + this.relative + ' only',
      buttons: [
        {
          text: 'Discard one from ' + this.relative,
          handler: () => {
            this.db.removeHabitHistory(this.habit.hash, this.habit.history[this.habit.history.length - 1].hash).then(done => {
              this.removedToast();
            });
          }
        },
        {
          text: 'Discard all from ' + this.relative
        }
      ]
    });
    sheet.present();
  }
  viewHabit() {
    this.nav.navigateForward('/habit/' + this.habit.hash);
  }
  async presentPopover(event) {

    const pop = await this.popover.create({
      component: HabitActionsPopoverComponent,
      event,
      mode: 'md',
      translucent: true,
      componentProps: {
        item: this.habit
      }
    });
    pop.present();
  }
  done() {
    const historyItem = new HabitHistory();
    historyItem.date = new Date();
    historyItem.isCompleted = true;
    historyItem.value = 1;
    this.db.addHabitHistory(this.habit.hash, historyItem).then(done => {
      this.ding.play();
    });
  }
  avoided() {
    this.presentRemoveOptions();
  }
  match() {
    this.results = this.validator.check(this.habit);
    if (this.results.length > 7) {
      this.results = this.results.slice(this.results.length - 7, this.results.length);
    }
    this.summary = this.validator.getSummary(this.habit);
    this.relative = this.summary.period.toRelativeFormat();

  }
  ngOnInit() {
    this.db.habits.subscribe(() => {
      this.match();
    });
  }

}
