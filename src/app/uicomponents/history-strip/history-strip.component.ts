import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Indicator, HabitIndicator, IndicatorStatus } from 'src/app/interfaces/indicator';
import { CheckResultItem, CheckResult } from 'src/app/pages/habit-wizard/habit-container/validator.service';
import { StatisticsService } from 'src/app/service/statistics.service';


@Component({
  selector: 'app-history-strip',
  templateUrl: './history-strip.component.html',
  styleUrls: ['./history-strip.component.scss'],
})
export class HistoryStripComponent implements OnInit, OnChanges {

  @Input() results: CheckResultItem[];
  indicators: Indicator[] = [];
  @Input() max = 7;
  constructor(private statService: StatisticsService) {

  }


  setIndicators() {

    this.indicators = [];
    this.results.forEach((item, index, array) => {
      const indicator = new HabitIndicator();
      if (item.verified) {
        indicator.status = IndicatorStatus.COMPLETED;
      } else if (item.result === CheckResult.NOT_STARTED) {
        indicator.status = IndicatorStatus.NOT_APPLICABLE;
      } else if (item.result === CheckResult.INSUFFICIENT) {
        indicator.status = IndicatorStatus.BLINKING;
      } else {
        indicator.status = IndicatorStatus.ERROR;
      }
      indicator.streak = this.statService.getStreakType(array[index - 1], item, array[index + 1]);
      this.indicators.push(indicator);
    });
  }
  ngOnInit() {
    this.setIndicators();
    


  }
  ngOnChanges(changes: SimpleChanges): void {
    this.setIndicators();
  }
}
