import { CheckResultItem } from 'src/app/pages/habit-wizard/habit-container/validator.service';

export class CalendarItem {
    date: Date;
    isCompleted: number;
    isRelevant = true;
    isBoundary: boolean;
    boundary: CheckResultItem;
}
