import { Injectable } from '@angular/core';
import { CalendarItem } from './calendar-item';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})

export class CalendarManagerService {
  MAX_YEAR = 2200;
  MIN_YEAR = 1970;
  /**
   * @description
   * Prepares date for a month in form of array of CalendarItem
   * @param month month of the year for this view
   * @param year The year for this view
   * @param items Items to map with this month
   */
  prepare(month: number, year: number): CalendarItem[] {
    let date = moment();
    const items: CalendarItem[] = [];
    if (this.isValidMonth(month)) {
      date = date.set('month', month);
    }
    if (this.isValidYear(year)) {
      date = date.set('year', year);
    }
    date = date.set('date', 1);
    const start = this.findStart(date);
    for (let i = 0; i <= (start[0] + start[1]); i++) {
      let dateItem = date.clone();
      if (i <= start[0]) {
        dateItem = dateItem.subtract(start[0] - i - 1, 'days');
      } else {
        dateItem = dateItem.add(i - start[0], 'day');
      }
      const item = new CalendarItem();
      item.date = dateItem.toDate();
      if (dateItem.toDate().getMonth() !== date.toDate().getMonth()) {
        item.isRelevant = false;
      }
      items.push(item);

    }
    return items;

  }
  /**
   * @returns number[] index 0 = Start of week day, index = 1 = days in month
   * @param selectedDate The date to get month and year for
   */
  findStart(selectedDate: moment.Moment): number[] {
    const days = selectedDate.daysInMonth();
    const startOfWeek = selectedDate.day();
    return [startOfWeek, days];
  }
  isValidYear(year: any): boolean {
    return (!year || isNaN(year) || year < this.MIN_YEAR || year > this.MAX_YEAR);
  }
  isValidMonth(month: any): boolean {
    return !isNaN(+month) && (+month <= 12 && +month >= 0);
  }
  constructor() { }
}
