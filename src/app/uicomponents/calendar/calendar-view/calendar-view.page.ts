import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.page.html',
  styleUrls: ['./calendar-view.page.scss'],
})
export class CalendarViewPage implements OnInit {
  days: number[] = [];
  constructor() { }

  ngOnInit() {
    for (let i = 0; i < 30; i++) {
      this.days.push(i);
    }
  }

}
