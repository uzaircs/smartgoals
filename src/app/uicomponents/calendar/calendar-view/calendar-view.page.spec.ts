import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalendarViewPage } from './calendar-view.page';

describe('CalendarViewPage', () => {
  let component: CalendarViewPage;
  let fixture: ComponentFixture<CalendarViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalendarViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
