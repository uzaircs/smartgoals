import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalendarViewPageRoutingModule } from './calendar-view-routing.module';

import { CalendarViewPage } from './calendar-view.page';
import { MonthViewComponent } from './month-view/month-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendarViewPageRoutingModule
  ],
  declarations: [CalendarViewPage, MonthViewComponent]
})
export class CalendarViewPageModule { }
