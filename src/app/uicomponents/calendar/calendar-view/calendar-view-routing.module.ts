import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalendarViewPage } from './calendar-view.page';
import { MonthViewComponent } from './month-view/month-view.component';

const routes: Routes = [
  {
    path: '',
    component: CalendarViewPage,
    children: [
      {
        path: 'month-view',
        component: MonthViewComponent
      },
      {
        path: '',
        redirectTo: 'month-view',
        pathMatch: 'full'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalendarViewPageRoutingModule { }
