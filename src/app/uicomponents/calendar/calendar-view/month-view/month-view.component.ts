import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CalendarItem } from '../../calendar-item';
import * as moment from 'moment';
import { CalendarManagerService } from '../../calendar-manager.service';


@Component({
  selector: 'app-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.scss'],
})
export class MonthViewComponent implements OnInit {
  activated = new CalendarItem();
  month: string;
  @Input() days: CalendarItem[] = [];
  @Output() selectionChanged = new EventEmitter();
  constructor(private manager: CalendarManagerService) { }
  activate(item: CalendarItem) {
    if (item.isRelevant) {
      this.activated = item;
      this.selectionChanged.emit(item);
    }
  }

  ngOnInit() {
    if (this.days) {
      this.month = moment(this.days[0].date).format('MMMM');
    }
  }
}


