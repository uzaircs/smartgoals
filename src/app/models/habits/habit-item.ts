import { RepeatItem, TimePeriod } from '../repeat/repeat-item';
import { ReminderItem } from '../reminder/reminder-item';
/**
 * An abstract class to automatically make any object unique 
 * @param hash The unique hash that will be calculated on instantiation of this class
 * This class must be inherited by other classes, any class that inherits this class will automatically have Unique {hash} as 
 * a property of its own
 */
export abstract class AutoUnique {
    hash: string;
    private generate() {
        let d = new Date().getTime();
        let d2 = (performance && performance.now && (performance.now() * 1000)) || 0;
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16;
            if (d > 0) {

                // tslint:disable-next-line: no-bitwise
                r = (d + r) % 16 | 0;
                d = Math.floor(d / 16);
            } else {
                // tslint:disable-next-line: no-bitwise
                r = (d2 + r) % 16 | 0;
                d2 = Math.floor(d2 / 16);
            }
            // tslint:disable-next-line: no-bitwise
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }
    constructor() {
        this.hash = this.generate();
    }
}
export class HistoryItem extends AutoUnique {
    date: Date = new Date();
    /**
     *  Create a new History item with Unique Hash identifier
     */
    constructor() {
        super();


    }
}
/**
 * Holds the stored value at a certain time for the user
 * @param value The value input by the user?
 * @param date The time on which this History item was added (Inherited from HistoryItem)
 * @param isCompleted wether this HabitItem was completed or avoided
 */
export class HabitHistory extends HistoryItem {
    value: number;
    isCompleted = false;
    constructor() {
        super();
    }

}
/**
 * Operator for value to win
 * 1= Exactly
 * 2 = Atleast 
 * 3 = Not more than
 */
export enum ConditionPolarity {
    EXACTLY = 1, // storedValue === specifiedValue
    ATLEAST, // storedValue >= specifiedValue
    NOT_MORE_THAN // storedValue < specifiedValue
}
/**
 * Win condition definition for a habit item
 * @param value the specified value that should match the stored value
 * @param polarity The polarity or comparing operator for the given value
 * @param isCompleted Specifies wether this habit should be avoided or completed in order for this condition to succeed
 */
export class WinCondition {
    value: number;
    polarity: ConditionPolarity;
    isCompleted = true;

}
/**
 * @description
 * The Model for **habit item**
 * > Habit item can be used to track progress of user habits, Remind user about their habits
 * Mantain a log of the users habits
 * 
 * Default **win condition** is set in **HabitContainerService** where Win Condition default polarity is *ConditionPolarity.ATLEAST* 
 * and default value is *1*. Which means by default 
 * > This habit should atleast be completed once per its **RepeatItem** specifications
 * 
 * Default **RepeatItem** specifications are also set in **HabitContainerService** where **interval** is *1* and 
 * **TimePeriod** is *TimePeriods.DAY* and **repetitions** = *1*
 * > This means by default This habit should be completed atleast 1 or more time(s) in 1 day in order for it to marked as succeeded
 * this can ofcourse be changed by changing the values of condition and repeatItem
 * 
 * @param hash Unique identifier for this habit item
 * @param name The name of the habit being tracked e.g Eat Apple
 * @param description optional description of this habit item
 * @param successDate The date on which this habit should be succesful
 * @param repeatItem To specifiy how many times this habit should be repeated in a timeframe in order to notify user to update Accordingly
 * @param method The tracking method of this habit 1 = Simple done/fail 2 = Specific Numbers
 * In method === 2 The WinCondition should be specified?
 * @param reminders Push notification reminders for this Habit item. This can be accessed by push service to schedule push notifications,
 * These should calculated automatically
 * @param history The history log containing how the user responded to this habit
 * @param condition The Win Condition of this Habit Item

 */
export class HabitItem extends AutoUnique {
    name: string;
    description: string;
    repetitions: number;
    successDate: Date;
    startDate: string;
    repeatItem: RepeatItem;
    method: number;
    reminders: ReminderItem[];
    history: HabitHistory[];
    condition: WinCondition;
}
export class Period {
    type: TimePeriod;
    value: number;
}