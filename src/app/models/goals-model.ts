import { Goal, Task } from '../service/database.service';
import { Observable, BehaviorSubject } from 'rxjs';

export class GoalModel implements Goal {
    Id: number;
    name: string;
    pre_req: number;
    repeat_type: boolean;
    start_date: Date;
    hash: string;
    end_date: Date;
    priority: number;
    status: number;
    deleted: boolean;
    created_at: Date;
    updated_at: Date;

    constructor(now = false) {
        if (now) {
            this.start_date = new Date();
        }

    }
    getTasks(): Observable<Task[]> {
        return new BehaviorSubject<Task[]>([]).asObservable();
    }
}