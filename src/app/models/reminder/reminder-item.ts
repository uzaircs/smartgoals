export class ReminderItem {
    hash: string;
    message: string;
    timestamp: Date;
    title: string;
    status: boolean;
}
