

/**
 * Repeat configuration item for task, habit and goal
 * @param repetitions The repetitions required in the specified interval
 * @param timePeriod The time frame type in which this item will be repeated [Example week, days, etc]?
 * @param interval interval of the specified timeframe e.g 7 + timePeriod = DAYS then 7 Days
 * Example 7 Repetitions required in 10 days can be specified as repetitions = 7, timePeriod = DAYS, interval = 10
 */
export class RepeatItem {
    repetitions: number;
    timePeriod: TimePeriod;
    interval: number;

    constructor() {

    }
}
/**
 * Basic time periods of calendar
 */
export enum TimePeriod {
    DAY = 1,
    WEEK,
    MONTH,
    YEAR
}

