import { Injectable } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
@Injectable({
  providedIn: 'root'
})
export class DingSoundService {

  constructor(private nativeAudio: NativeAudio) {

  }
  play() {
    console.log('loading audio');
    this.nativeAudio.preloadSimple('ding', 'assets/sounds/ding/candle.wav').then(s => {
      console.log('playing audio');
      this.nativeAudio.play('ding');
    }).catch(err => {
      this.nativeAudio.play('ding');
    });

  }
}
