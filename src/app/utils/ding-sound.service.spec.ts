import { TestBed } from '@angular/core/testing';

import { DingSoundService } from './ding-sound.service';

describe('DingSoundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DingSoundService = TestBed.get(DingSoundService);
    expect(service).toBeTruthy();
  });
});
