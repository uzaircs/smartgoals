import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { HttpClient } from '@angular/common/http';
import { StorageEngine } from '../interfaces/storage-engine';
import { Habit } from '../interfaces/habit';

export interface Goal {
  Id: number;
  hash: string;
  name: string;
  pre_req: number;
  repeat_type: boolean;
  start_date: Date;
  end_date: Date;
  priority: number;
  status: number;
  created_at: Date;
  updated_at: Date;
  deleted: boolean;

}
export interface Task {
  deleted: boolean;
  Id: number;
  hash: string;
  name: string;
  goal: string;
  repeat_type: number; // 0= false 1 =yes/repeating
  repeat_every: number; // 1 =day 2 = week, 3 = month,4 = everyweekday, 5 = custom
  status: number; // 0 = pending,1 = doing, 2 = completed
  priority: number;
  due_date: Date;
  reminder: Date;
  task_value: number;
  estimated_time: number;
  milestone: number;
  notes: string;
}
export class TaskModel implements Task {
  Id: number; hash: string;
  name: string;
  goal: string;
  repeat_type: number;
  repeat_every: number;
  status: number;
  priority: number;
  due_date: Date;
  reminder: Date;
  deleted: boolean;
  task_value: number;
  estimated_time: number;
  milestone: number;
  notes: string;


}
@Injectable({
  providedIn: 'root'
})

export class DatabaseService {

  Goals = new BehaviorSubject([]);
  Tasks = new BehaviorSubject([]);
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  deleteGoal(Id: number): Promise<boolean> {
    return new Promise(r => r());
  }
  getGoal(id: number): Promise<Goal> {
    return new Promise(r => r());
  }
  updateGoal(Id: number, goal: Goal): Promise<Goal> {
    return new Promise(r => r());
  }
  addTask(task: Task): Promise<Task> {
    return new Promise(r => r());
  }
  getTasks(goalId?: number): Observable<Task[]> {
    return new BehaviorSubject<Task[]>([]).asObservable();
  }
  deleteTask(Id: number): Promise<boolean> {
    return new Promise(r => r());
  }
  updateTask(hash: string, newTask: Task): Promise<Task> {
    return new Promise(r => r());
  }
  addHabit(habit: Habit): Promise<Habit> {
    return new Promise(r => r());
  }
  updateHabit(Id: number, habit: Habit): Promise<Habit> {
    return new Promise(r => r());
  }
  deleteHabit(Id: number): Promise<boolean> {
    return new Promise(r => r());
  }


  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient) {
    this.plt.ready().then(x => {

      if (!this.plt.is('desktop')) {
        this.sqlite.create({
          name: 'smartgoals.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
          this.dbReady.next(true);
        });
      }
    });
  }
  getDatabaseState() {
    return this.dbReady.asObservable();
  }
  seedDatabase() {
    this.http.get('assets/smartgoals.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(_ => {
            this.loadGoals();
            /*  this.loadTasks(); */
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }
  addGoal(goal: Goal): Promise<Goal> {
    const promise = new Promise<Goal>((resolve, reject) => {
      this.database.
        executeSql(
          `INSERT INTO goal 
        (name,created_at,updated_at,pre_req,repeat_type,start_date,end_date,status,priority) 
        VALUES (?,?,?,?,?,?,?,?,?)`,
          [
            goal.name,
            goal.created_at,
            goal.updated_at,
            goal.pre_req,
            goal.repeat_type,
            goal.start_date,
            goal.end_date,
            goal.status,
            goal.priority
          ]).then(data => {
            resolve(data);
            this.loadGoals();
          }).catch(err => reject(err));
    });
    return promise;

  }
  getGoals(): Observable<Goal[]> {
    return this.Goals.asObservable();
  }
  loadGoals() {
    return this.database.executeSql('SELECT * FROM goal', []).then(data => {
      const goals: Goal[] = [];
      if (data.rows.length > 0) {

        for (let i = 0; i < data.rows.length; i++) {
          const item = data.rows.item(i);


          goals.push({
            Id: item.Id,
            created_at: new Date(),
            end_date: item.end_date,
            name: item.name,
            hash: item.hash,
            repeat_type: item.repeat_type,
            pre_req: item.pre_req,
            priority: item.priority,
            start_date: item.start_date,
            status: item.status,
            updated_at: item.updated_at,
            deleted: false
          });

        }
      }
      this.Goals.next(goals);

    });
  }
}
