import { Injectable } from '@angular/core';
import { StorageEngine, DbKeys } from '../interfaces/storage-engine';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Goal, Task } from './database.service';
import { GuidService } from './guid.service';
import { HabitItem, HistoryItem, HabitHistory } from '../models/habits/habit-item';
import { Pomodoro, IPomodoro, TimeElapsed } from '../pages/timer/pomodoro/models/ipomodoro';
@Injectable({
  providedIn: 'root'
})
export class GlobalStorageService implements StorageEngine {


  goals = new BehaviorSubject<Goal[]>([]);
  tasks = new BehaviorSubject<Task[]>([]);
  habits = new BehaviorSubject<HabitItem[]>([]);
  $habits = this.habits.asObservable();
  $tasks = this.tasks.asObservable();
  $goals = this.goals.asObservable();
  timers = new BehaviorSubject<Pomodoro[]>([]);
  $timers = this.timers.asObservable();
  timerQueue = new BehaviorSubject<IPomodoro[]>([]);
  $timerQueue = this.timerQueue.asObservable();
  dbReady = new BehaviorSubject<boolean>(false);
  saveTimer(timer: Pomodoro, clearQueue = true): Promise<Pomodoro> {

    return new Promise((resolve, reject) => {
      if (!timer) {
        reject('No timer specified');
      }
      this.dbReady.subscribe(ready => {
        if (ready) {
          let timers = this.timers.getValue();
          if (timers || !timers.length) {
            timers = [];
          }
          timers.push(timer);
          this.saveTimerArray(timers).then(saved => {
            if (saved) {
              if (clearQueue) {
                this.clearTimerQueue().then(done => {
                  if (done) {
                    resolve(timer);
                  }
                }).catch(err => reject(err));
              } else {
                resolve(timer);
              }
              this.timers.next(timers);
            }
          }).catch(err => reject(err));
        }
      });
    });
  }
  queueTimer(timer: Pomodoro): Promise<Pomodoro> {

    return new Promise((resolve, reject) => {
      this.storage.get(DbKeys.PENDING_TIMERS).then(timers => {
        let timers$ = [];
        if (timers) {
          timers$ = JSON.parse(timers);
        }
        if (timers$.length === 0) {
          const pendingTimers: IPomodoro[] = [timer].map<IPomodoro>(d => {
            return {
              elapsed: d.elapsed,
              passed: d.passed,
              paused: d.paused,
              required: d.required,
              start: (): Observable<TimeElapsed> => null,
              stop: (): Promise<TimeElapsed> => null,
              toggle: (): boolean => false
            };
          });
          this.storage.set(DbKeys.PENDING_TIMERS, JSON.stringify(pendingTimers)).then(done => {
            resolve(timer);
            this.timerQueue.next(pendingTimers);
          }).catch(err => reject(err));
        } else if (timers$.length >= 1) {
          reject({ code: 1, message: 'A Timer is already in que', data: { timer: timers$ } });
        }
      });
    });

  }
  getTimerQueue(): Observable<IPomodoro[]> {
    return this.$timerQueue;
  }
  validateTimerQueue(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.storage.get(DbKeys.PENDING_TIMERS).then(timers => {
        const timers$ = JSON.parse(timers);
        if (timers$ && timers$.length && timers$.length > 1) {
          reject({ code: 2, message: `Queue contains ${timers$.length} items` });
        } else if (timers$.length === 1) {
          resolve(1);
        } else {
          resolve(0);
        }
      });
    });
  }
  clearTimerQueue(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.set(DbKeys.PENDING_TIMERS, '').then(done => {
        resolve(true);
        this.timerQueue.next([]);
      }).catch(err => reject(err));
    });

  }
  getTimers(): Observable<Pomodoro[]> {
    return this.$timers;
  }
  getTimer(hash: string): Promise<Pomodoro> {
    return new Promise((resolve, reject) => {
      const timers = this.timers.getValue();
      const timer$ = timers.find(t => t.hash === hash);
      if (timer$) {
        resolve(timer$);
      } else {
        reject('Timer not found');
      }
    });
  }
  private saveTimerArray(timers: Pomodoro[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (timers && timers.length) {
        this.storage.set(DbKeys.SAVED_TIMERS, JSON.stringify(timers)).then(done => {
          resolve(true);
        }).catch(err => reject(err));
      }
    });
  }
  getGoal(hash: string): Promise<Goal> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {

        if (ready) {
          if (this.goals.getValue() && this.goals.getValue().length > 0) {
            const goal = this.goals.getValue().find(g => g.hash === hash);
            if (goal) {
              resolve(goal);
            } else {
              reject('Goal Not Found');
            }
          }
        }
      });
    });

  }
  addGoal(goal: Goal): Promise<Goal> {

    return new Promise<Goal>((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          let currentGoals = this.goals.getValue();
          if (!currentGoals) {
            currentGoals = [];
          }
          goal.hash = this.guid.generate();
          let length = (currentGoals && currentGoals.length) || 1;
          do {
            goal.Id = length;
          } while (currentGoals && currentGoals[length++]);
          currentGoals.push(goal);
          this.storage.set(DbKeys.GOALS, JSON.stringify(currentGoals)).then(s => {
            resolve(s);
            this.goals.next(currentGoals);

          }).catch(err => reject(err));
        }
      });
    });

  }
  getGoals(): Observable<Goal[]> {
    return this.$goals.pipe(map((goals: Goal[]) => goals.filter(g => !g.deleted)));
  }
  deleteGoal(hash: string): Promise<boolean> {

    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          this.getGoal(hash).then(g => {
            g.deleted = true;
            this.updateGoal(hash, g).then(done => {
              resolve(true);
            });
          });
        }
      });
    });
  }
  updateGoal(hash: string, goal: Goal): Promise<Goal> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          const currentGoals = this.goals.value;
          const index = currentGoals.findIndex(i => i.hash === hash);
          if (currentGoals[index]) {
            currentGoals[index] = goal;
            this.storage.set(DbKeys.GOALS, JSON.stringify(currentGoals)).then(done => {
              resolve(goal);
              this.goals.next(currentGoals);
            }).catch(err => reject(err));
          } else { reject('Goal Not Found'); }
        }
      });
    });
  }
  private setTask(tasks: Task[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          this.storage.set(DbKeys.TASKS, JSON.stringify(tasks)).then(done => {
            resolve(JSON.parse(done));
          }).catch(err => { reject(err); });
        }
      })
    });
  }
  addTask(task: Task): Promise<Task> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          let currentTasks = this.tasks.getValue();
          if (!currentTasks) {
            currentTasks = [];
          }
          task.hash = this.guid.generate();
          currentTasks.push(task);
          this.setTask(currentTasks).then(done => {
            this.tasks.next(currentTasks);
            resolve(done);
          }).catch(err => reject(err));
        }
      });
    });
  }

  getTasks(): Observable<Task[]> {
    return this.$tasks.pipe(map((tasks: Task[]) => tasks ? tasks.filter(t => !t.deleted) : []));
  }
  deleteTask(hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          const index = this.tasks.getValue().findIndex(t => t.hash === hash);
          if (index >= 0) {
            const task = this.tasks.getValue()[index];
            const updatedTasks = this.tasks.getValue();
            if (task) {
              task.deleted = true;
            }
            updatedTasks[index] = task;
            this.setTask(updatedTasks).then(done => {
              this.tasks.next(updatedTasks);
              this.goals.next(this.goals.getValue());
              resolve(done);
            }).catch(err => reject(err));
          }
        }
      });
    });
  }
  updateTask(hash: string, newTask: Task): Promise<Task> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          const t = this.tasks.getValue();
          if (t && t.length) {
            const index = t.findIndex(i => i.hash === hash);
            if (t[index]) {
              t[index] = newTask;
              this.setTask(t).then(done => {
                this.tasks.next(t);
                this.goals.next(this.goals.getValue());
                resolve(done);
              }).catch(err => reject(err));
            } else { reject('Task Not Found'); }
          }
        }
      });
    });
  }
  private setHabits(habits: HabitItem[]): Promise<HabitItem[]> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          this.storage.set(DbKeys.HABITS, JSON.stringify(habits)).then(done => {
            resolve(JSON.parse(done));
          }).catch(err => { reject(err); });
        }
      });
    });

  }
  addHabitHistory(hash: string, item: HabitHistory): Promise<HabitItem> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(isReady => {
        if (isReady) {
          const habits = this.habits.getValue();
          if (!habits) {
            reject('No habits');
          }
          const index = habits.findIndex(habit => habit.hash === hash);
          if (!isNaN(index) && index >= 0) {
            const habitItem: HabitItem = habits[index];
            if (!habitItem.history || !habitItem.history.length) {
              habitItem.history = [];
            }
            habitItem.history.push(item);

            habits[index] = habitItem;
            this.setHabits(habits);
            this.habits.next(habits);
            resolve(habitItem);
          }
        }
      });
    });
  }
  removeHabitHistory(habitHash: string, Historyhash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const habits = this.habits.getValue();
      const selectedHabitIndex = habits.findIndex(x => x.hash === habitHash);
      if (habits[selectedHabitIndex] && habits[selectedHabitIndex].history && habits[selectedHabitIndex].history.length > 0) {
        const historyIndex = habits[selectedHabitIndex].history.findIndex(h => h.hash === Historyhash);
        habits[selectedHabitIndex].history.splice(historyIndex, 1);
        this.setHabits(habits);
        this.habits.next(habits);
        resolve(true);
      } else {
        reject('Habit or history not found');
      }
    });
  }
  addHabit(habit: HabitItem): Promise<HabitItem> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(isReady => {
        if (isReady) {
          let habits = this.habits.getValue();
          if (!habits) {
            habits = [];
          }
          habits.push(habit);
          this.setHabits(habits).then(done => {
            this.habits.next(habits);
            resolve(habit);
          }).catch(err => reject(err));
        }
      });

    });
  }
  /**
   * @description
   * Removes all history of a habit item
   * @param {habit} habit the habit item
   */
  resetHabitHistory(habit: HabitItem): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (habit) {
        habit.history = [];
        this.updateHabit(habit.hash, habit).then(done => {
          resolve(true);
        }).catch(err => reject(err));
      } else {
        reject('Habit not specified');
      }
    });

  }
  /**
   * @description 
   * Update the habit in storage
   * @param hash the unique identifier for this habit
   * @param habit The habit item
   */
  updateHabit(hash: string, habit: HabitItem): Promise<HabitItem> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(ready => {
        if (ready) {
          const habits = this.habits.getValue();
          if (!habits) {
            reject('No habits currently');
          } else {
            const index = habits.findIndex(h => h.hash === hash);
            if (index < 0) {
              reject('No habits currently');

            } else {
              habits[index] = habit;
              this.setHabits(habits);
              this.habits.next(habits);
              resolve(habit);
            }
          }
        }
      });

    });
  }
  getHabits(): Observable<HabitItem[]> {
    return this.$habits;
  }
  deleteHabit(hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.dbReady.subscribe(isReady => {
        if (isReady) {
          let habits = this.habits.getValue();
          if (habits) {
            reject('No habits currently');
          } else {
            const index = habits.findIndex(h => h.hash === hash);
            if (index < 0) {
              reject('No habits currently');
            } else {
              habits = habits.filter(h => h.hash !== hash);
              this.habits.next(habits);
              resolve(true);

            }
          }
        }
      });
    });
  }

  constructor(private storage: Storage, private guid: GuidService) {
    this.storage.ready().then(db => {
      const promises = [];
      const $getGoals = this.storage.get(DbKeys.GOALS).then(goals => {
        this.goals.next(JSON.parse(goals));
      });
      const $getTask = this.storage.get(DbKeys.TASKS).then(tasks => {
        this.tasks.next(JSON.parse(tasks));
      });
      const $getHabits = this.storage.get(DbKeys.HABITS).then(habits => {
        this.habits.next(JSON.parse(habits));
      });
      const $getTimers = this.storage.get(DbKeys.SAVED_TIMERS).then(timers => {
        this.timers.next(JSON.parse(timers));
      });
      const $getQueue = this.storage.get(DbKeys.PENDING_TIMERS).then(timers => {
        if (timers) {
          this.timerQueue.next(JSON.parse(timers));
        }

      });
      promises.push($getGoals, $getHabits, $getTask, $getTimers, $getQueue);
      Promise.all(promises).then(done => {
        this.dbReady.next(true);
      }).catch(err => {
        console.log(err);
      });
    });
  }
}
