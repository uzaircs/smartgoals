import { Injectable } from '@angular/core';
import { SettingItem } from '../interfaces/settings-item';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private DB_SETTINGS = '___settings';
  settings: SettingItem[];
  $settings = new BehaviorSubject<SettingItem[]>([]);
  constructor(private db: Storage) {
    this.db.ready().then(ready => {
      this.db.get(this.DB_SETTINGS).then(data => {
        this.settings = JSON.parse(data);
        this.$settings.next(this.settings);
      });
    });
  }
  save(settings: SettingItem) {
    const exists = this.settings.findIndex(item => item.name === settings.name);
    if (exists >= 0) {
      this.settings.splice(exists, 1);
    }
    this.settings.push(settings);
    this.db.set(this.DB_SETTINGS, JSON.stringify(settings));
    this.$settings.next(this.settings);
  }
  get(): Observable<SettingItem[]> {
    return this.$settings.asObservable();
  }

}
