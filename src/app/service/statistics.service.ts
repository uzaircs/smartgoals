import { Injectable } from '@angular/core';
import { GlobalStorageService } from './global-storage.service';
import { Goal } from './database.service';
import { GoalStatistics } from '../interfaces/goal-statistics';
import { HabitItem } from '../models/habits/habit-item';
import {
  ValidatorService,
  CheckResultItem,
  PeriodFormatter,
  TimePeriodItem,
  CheckResult
} from '../pages/habit-wizard/habit-container/validator.service';
import * as moment from 'moment';
import { IndicatorStatus } from '../interfaces/indicator';
import { TimePeriod } from '../models/repeat/repeat-item';
export class DateRange {
  startDate: Date;
  endDate: Date;
}
export class BreakdownItem {
  condition: string;
  date: string;
  $date: Date;
  $range: DateRange;
  totalDays: number;
  totalCompleted: number;
  totalFailed: number;
  current: number;
  currentStatus: CheckResult;
  isRange = false;
}
export class StreakItem {
  startDate: Date;
  endDate: Date;
  streakValue: number;
}
export enum StreakType {
  NONE,
  BEGIN = 1,
  CENTER,
  END,
}
@Injectable({
  providedIn: 'root'
})
/**
 * Calculate user performance for feedback
 */
export class StatisticsService {
  /**
   * @description
   * Gets a simple day breakdown of a habit showing statistics in `BreakdownItem`
   * @param habit The habit item to get the breakdown for
   * @param date The date which has been selected for the breakdown
   */
  getHabitBreakDown(habit: HabitItem, date: Date): BreakdownItem {
    if (moment(date).isValid()) {
      const item = new BreakdownItem();
      item.$date = date;
      item.totalDays = (moment(habit.startDate).diff(moment(), 'days') * -1) + 1;
      const timePeriod = new TimePeriodItem(habit);
      item.condition = timePeriod.toString();
      item.$range = this.getRange(date, habit);
      const results = this.validator.check(habit);
      item.isRange = (habit.repeatItem.interval > 1 || habit.repeatItem.timePeriod !== TimePeriod.DAY);
      item.totalCompleted = results.filter(x => (x.date >= moment(habit.startDate).toDate()) && x.date < date && x.verified).length;
      item.totalFailed = results.filter(x => (x.date >= moment(habit.startDate).toDate() && x.date < date) && !x.verified).length;
      let key: string;
      const sequence = this.validator.mapSequence
        (this.validator.generateSequence
          (moment(habit.startDate).toDate(), habit.repeatItem, date), habit.history);
      results.forEach(result => {
        if ((habit.repeatItem.interval > 1 && habit.repeatItem.timePeriod === TimePeriod.DAY)
          || habit.repeatItem.timePeriod !== TimePeriod.DAY) {
          key = this.validator.findMedianKey(sequence, date);


        } else {
          key = moment(date).format(this.validator.DATE_FORMAT);
        }

      });

      item.current = sequence[key].filter(s => s).length;
      return item;
    }
  }
  /**
   * @description 
   * Gets the date range of a habit item on a specific date i.e what range does the specified date falls in. Applicable where 
   * HabitItem has interval > 1 || interval is not days
   * @param date The date to get DateRange from
   * @param item The habit item to get dateRange of
   * @example
   * getRange(2/2/2020,item) // -> Where item has interval of 4 days 
   * Output :
   * {
   *  startDate : 1/1/2020,
   *  endDate : 4/1/202
   * }
   */
  getRange(date: Date, item: HabitItem): DateRange {
    const sequence = this.validator.generateSequence(item.startDate, item.repeatItem, date);
    const key = this.validator.findMedianKey(sequence, date);
    if (key) {
      const endDate = this.validator.parseKey(key);
      const startDate = moment(endDate)
        .subtract(item.repeatItem.interval, this.validator.periodToString(item.repeatItem.timePeriod))
        .add('1', 'days')
        .toDate();
      const range = new DateRange();
      range.startDate = startDate;
      range.endDate = endDate;
      return range;
    } else {
      return null;
    }
  }
  constructor(private db: GlobalStorageService, private validator: ValidatorService) { }
  /**
   * Get statistics of a single goal
   * @param goal The goal to calculate stats for
   */
  goalStats(goal: Goal): Promise<GoalStatistics> {
    return new Promise<GoalStatistics>((resolve, reject) => {
      const statistics = new GoalStatistics();
      this.db.getTasks().subscribe(tasks => {
        try {
          const goalTasks = tasks.filter(task => task.goal === goal.hash);
          const completedTasks = goalTasks.filter(Task => Task.status === 2);
          statistics.completedTasks = completedTasks.length;
          statistics.totalTasks = goalTasks.length;
          statistics.totalMilestones = goalTasks.filter(Task => Task.milestone === 1).length;
          statistics.remainingTasks = statistics.totalTasks - statistics.completedTasks;
          statistics.totalTime = goalTasks.map(Task => +Task.estimated_time * 60 || 0).concat([0]).reduce((a, b) => +a + +b);

          const averageTime =
            completedTasks
              .filter(time => +time.estimated_time > 0)
              .map(time => +time.estimated_time)
              .concat([0])
              .filter(n => !isNaN(n))
              .reduce((a, b) => +a + +b) / completedTasks.filter(time => (time.estimated_time * 60) > 0).length;

          statistics.estimatedTimeSpent =
            completedTasks
              .map(Task => ((+Task.estimated_time * 60)))
              .concat([0])
              .filter(n => !isNaN(n))
              .reduce((a, b) => +a + +b);

          statistics.completedPercent = (statistics.completedTasks / statistics.totalTasks) * 100;
          resolve(statistics);
        } catch (error) {
          reject(error);
        }
      });
    });
  }
  private getStreakEndIndex(startIndex: number, results: CheckResultItem[]): number {
    let index = results.slice(startIndex).findIndex(item => !item.verified);
    index--;
    return (index >= 0 ? (startIndex + index) : results.length - 1);
  }
  getStreakType(prev: CheckResultItem, current: CheckResultItem, next: CheckResultItem): StreakType {
    if (current.verified && (prev && prev.verified) && (next && next.verified)) {
      return StreakType.CENTER;
    }
    if (current.verified && (prev && !prev.verified) && (next && next.verified)) {
      return StreakType.BEGIN;
    }
    if (current.verified && (prev && prev.verified) && (!next || !next.verified)) {
      return StreakType.END;
    }
    return StreakType.NONE;
    if (!current.verified) {

    }
  }
  /**
   * @description
   * Get streaks of a habit item
   * @param habit The habitItem to get streak for
   * @returns StreakItem[]
   */
  GetHabitStreaks(habit: HabitItem): StreakItem[] {
    const items: StreakItem[] = [];
    const results = this.validator.check(habit);
    for (let i = 0; i < results.length - 1;) {
      const current = results[i];
      const previous = results[i - 1];
      const next = results[i + 1];
      const streak = this.getStreakType(previous, current, next);

      if (streak !== StreakType.NONE) {
        const item = new StreakItem();
        const streakEndIndex = this.getStreakEndIndex(i, results);
        item.startDate = current.date;
        item.endDate = results[streakEndIndex].date;
        item.streakValue = (streakEndIndex - i + 1);
        items.push(item);
        i = streakEndIndex + 1;

      } else {
        i++;
      }


    }
    return items;
  }


}

