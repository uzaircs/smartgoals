import { Injectable } from '@angular/core';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { BehaviorSubject } from 'rxjs';
import { INotification } from './INotification';
@Injectable({
  providedIn: 'root'
})
export class PushService {
  $ready = new BehaviorSubject<boolean>(false);
  constructor(private notificationService: LocalNotifications) {
    notificationService.hasPermission().then(permission => {
      if (permission.valueOf()) {
        this.$ready.next(true);
      }
    });
  }
  send(notification: INotification) {
    this.notificationService.schedule({
      foreground: true,
      launch: true,
      text: notification.message,
      title: notification.title,
      trigger: {
        in: 5,
        unit: ELocalNotificationTriggerUnit.SECOND
      }
    });
  }
}
