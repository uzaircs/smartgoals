import { Injectable } from '@angular/core';
import { StorageEngine } from '../interfaces/storage-engine';
import { Goal, Task, DatabaseService } from './database.service';
import { Observable } from 'rxjs';
import { Habit } from '../interfaces/habit';
import { Platform } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
/**
 * Outdated service will be removed soon
 */
export class StorageService {

  db: StorageEngine;
  getGoal(hash: string): Promise<Goal> {
    return this.db.getGoal(hash);
  }
  addGoal(goal: Goal): Promise<Goal> {
    const handler = new Promise<Goal>((resolve, reject) => {
      try {
        this.db.addGoal(goal).then(g => {
          resolve(g);
        }).catch(e => reject(e));

      } catch (error) {
        reject(error);
      }
    });
    return handler;
  }
  getGoals(): Observable<Goal[]> {
    return this.db.getGoals();
  }


}
