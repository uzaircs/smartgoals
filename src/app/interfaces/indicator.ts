import { StreakType } from '../service/statistics.service';


export interface Indicator {
    status: IndicatorStatus;
}
export class HabitIndicator implements Indicator {
    status: IndicatorStatus;
    streak: StreakType;

}
export enum IndicatorStatus {
    PENDING = 0,
    COMPLETED,
    ERROR,
    NOT_APPLICABLE,
    BLINKING,
    INCOMPLETE
}
