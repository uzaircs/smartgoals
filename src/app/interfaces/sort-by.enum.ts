import { Task } from '../service/database.service';

export enum SortBy {
    DUE_DATE = 0,
    STATUS,
    PRIORITY,

}
export class TaskSort {
    static ascDueDate(taskA: Task, taskb: Task) {

        taskA.due_date = new Date(taskA.due_date);
        taskb.due_date = new Date(taskb.due_date);
        if (taskA.due_date && taskb.due_date) {
            return (taskA.due_date.getTime() - taskb.due_date.getTime());
        } else if (taskA.due_date) {
            return 1;
        } else if (taskb.due_date) {
            return -1;
        }
        return 0;


    }
    static descDueDate(taskA: Task, taskb: Task): number {
        return (taskb.due_date.getTime() - taskA.due_date.getTime());
    }
}
