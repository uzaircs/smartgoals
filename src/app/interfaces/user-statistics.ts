import { GlobalStorageService } from '../service/global-storage.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { GoalStatistics } from './goal-statistics';
import { StatisticsService } from '../service/statistics.service';
import { filter } from "rxjs/operators";
export class UserStatistics {
    private goalStats: GoalStatistics[] = [];
    private $goalStats: BehaviorSubject<GoalStatistics[]> = new BehaviorSubject([]);
    private $$goalStats = this.$goalStats.asObservable();
    /**
     * Combine all statistics of user
     */
    constructor(private db: GlobalStorageService, private statsFactory: StatisticsService) {
        this.db.getGoals().subscribe(goals => {
            const promises: Promise<any>[] = [];
            goals.forEach(goal => {
                promises.push(statsFactory.goalStats(goal).then(stats => this.goalStats.push(stats)));
            });
            Promise.all(promises).then(done => {
                this.$goalStats.next(this.goalStats);
            });
        });

    }
    GetStatistics(): Observable<GoalStatistics[]> {
        return this.$$goalStats;
    }
}
