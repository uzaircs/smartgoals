import { Goal, Task } from '../service/database.service';
import { Observable } from 'rxjs';
import { Habit } from './habit';
import { HabitItem } from '../models/habits/habit-item';


export interface StorageEngine {

    addGoal(goal: Goal): Promise<Goal>;
    getGoal(hash: string): Promise<Goal>;
    getGoals(): Observable<Goal[]>;
    deleteGoal(hash: string): Promise<boolean>;
    updateGoal(hash: string, goal: Goal): Promise<Goal>;
    addTask(task: Task): Promise<Task>;
    getTasks(goalHash?: string): Observable<Task[]>;
    deleteTask(hash: string): Promise<boolean>;
    updateTask(hash: string, newTask: Task): Promise<Task>;
    addHabit(habit: HabitItem): Promise<HabitItem>;
    updateHabit(hash: string, habit: HabitItem): Promise<HabitItem>;
    getHabits(): Observable<HabitItem[]>;
    deleteHabit(hash: string): Promise<boolean>;
}
export enum DbKeys {
    GOALS = 'goals',
    HABITS = 'habits',
    TASKS = 'tasks',
    SAVED_TIMERS = 'timers',
    PENDING_TIMERS = 'pending_timers'
}
