

export enum ActivityType {
    TASK_CREATED = 1,
    TASK_DELETED, TASK_UPDATED, TASK_DONE, TASK_TIME_SPENT,
    GOAL_CREATED, GOAL_UPDATED, GOAL_DELETED, GOAL_COMPLETED,

}
/**
 * Model for Goal overview and performance calculation
 */
export class GoalStatistics {
    totalTasks: number;
    completedPercent: number;
    totalMilestones: number;
    completedTasks: number;
    lastTaskTime: Date;
    remainingTasks: number;
    totalTime: number;
    estimatedTimeSpent: number;
    estimatedTimeRemaining: number;
}
export class TaskStatistics {
    hash: string;
    activities: Activity[];

}
/**
 * Draft activity class for debugging
 */
export class Activity {
    activityType: ActivityType;
    timestamp: Date;
}


