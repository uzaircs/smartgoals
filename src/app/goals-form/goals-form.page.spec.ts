import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoalsFormPage } from './goals-form.page';

describe('GoalsFormPage', () => {
  let component: GoalsFormPage;
  let fixture: ComponentFixture<GoalsFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalsFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoalsFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
