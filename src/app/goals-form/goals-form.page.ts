import { Component, OnInit } from '@angular/core';
import { DatabaseService, Goal } from '../service/database.service';
import { GoalModel } from '../models/goals-model';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { GlobalStorageService } from '../service/global-storage.service';
import * as moment from 'moment';
@Component({
  selector: 'app-goals-form',
  templateUrl: './goals-form.page.html',
  styleUrls: ['./goals-form.page.scss', '../header.scss'],
})
export class GoalsFormPage implements OnInit {

  goal: GoalModel = new GoalModel();
  $goal: any = {
    name: true,
    end_date: true,
    start_date: true
  };
  constructor(private db: GlobalStorageService, private router: Router, private toast: ToastController) { }
  ngOnInit() {
    setTimeout(() => {
      this.goal.start_date = new Date();
    }, 1000);
  }
  async validationToast(message: string) {
    const toast = await this.toast.create({
      message,
      header: 'Please check',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {

      if (!this.goal.name) {
        this.$goal.name = false;
        reject('Goal name missing');
      } else {
        this.$goal.name = true;
      }
      if (!this.goal.end_date) {
        reject('End date missing');
      }
      if (!this.goal.start_date) {
        reject('Start date missing');
      }
      if (!this.goal.name) {
        reject('Goal name missing');
      }
      resolve(true);
    });

  }
  async presentToast() {
    const toast = await this.toast.create({
      message: 'Goal Added',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  save() {
    this.validate().then(done => {
      this.db.addGoal(this.goal).then(done => {

        this.presentToast();
        this.router.navigateByUrl('');

      }).catch(error => {
        console.log(error);
      });
    }).catch(err => {
      this.validationToast(err);
    });
  }

}

