import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoalsFormPageRoutingModule } from './goals-form-routing.module';

import { GoalsFormPage } from './goals-form.page';
import { UIComponentsModule } from '../uicomponents/uicomponents.module';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UIComponentsModule,
    IonicSelectableModule,
    GoalsFormPageRoutingModule
  ],
  declarations: [GoalsFormPage]
})
export class GoalsFormPageModule {}
