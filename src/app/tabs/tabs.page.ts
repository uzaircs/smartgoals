import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HabitContainerService } from '../pages/habit-wizard/habit-container/habit-container.service';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public actionSheetController: ActionSheetController, private router: Router, private container: HabitContainerService) { }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Create New',
      buttons: [{
        text: 'Habit',
        icon: 'repeat-outline',
        handler: () => {
          this.container.isUpdating = false;
          this.router.navigateByUrl('habit-wizard');
        }
      }, {
        text: 'Goal',
        icon: 'custom-goal',
        handler: () => {
          this.router.navigateByUrl('goals-form');
        }
      }, {
        text: 'Milestone',
        icon: 'custom-flag',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Label',
        icon: 'custom-label',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
