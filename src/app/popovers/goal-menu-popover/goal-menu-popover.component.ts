import { Component, OnInit } from '@angular/core';
import { ToastController, PopoverController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Goal } from 'src/app/service/database.service';
import { GlobalStorageService } from 'src/app/service/global-storage.service';

@Component({
  selector: 'app-goal-menu-popover',
  templateUrl: './goal-menu-popover.component.html',
  styleUrls: ['./goal-menu-popover.component.scss'],
})
export class GoalMenuPopoverComponent implements OnInit {
  goal: Goal;
  constructor(
    private toast: ToastController,
    private router: Router,
    private popover: PopoverController,
    private db: GlobalStorageService,
    private alert: AlertController
  ) { }
  async presentAlert() {
    this.popover.dismiss();
    const alert = await this.alert.create({
      backdropDismiss: false,
      header: 'Confirm Delete',
      subHeader: 'All tasks for this goal including goal itself will be deleted',
      message: 'Are you sure you want to delete this goal?',
      buttons: [{
        text: 'No',
        handler: () => {
          this.alert.dismiss();
        }

      },
      {
        text: 'Confirm Delete',
        /*  role: 'destructive', */
        handler: () => {
          this.delete().then(done => {
            if (done) {
              this.deleteToast();
            }
          });
        }
      }
      ]
    });

    await alert.present();
  }
  async  pauseGoal() {
    this.popover.dismiss();
    const toast = await this.toast.create({
      message: 'Goal paused',
      duration: 2000,
      color: 'tertiary'
    });
    toast.present();
  }
  async deleteToast() {
    const toast = await this.toast.create({
      message: 'Goal was deleted succesfuly',
      header: 'Goal Deleted',
      color: 'success',
      buttons: [{
        side: 'end',
        text: 'Undo',
        handler: () => {
          this.undoDelete().then(done => {
            this.presentUndoToast();
          });
        }
      }],
      duration: 3000

    });
    toast.present();
  }
  async presentUndoToast() {
    const toast = await this.toast.create({
      message: 'Goal restored',
      color: 'primary',
      duration: 1500
    });
    toast.present();
  }
  undoDelete(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.goal.deleted = false;
      this.db.updateGoal(this.goal.hash, this.goal).then(done => {
        resolve(true);
      }).catch(err => reject(err));
    });

  }
  delete(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.goal.deleted = true;
      this.db.deleteGoal(this.goal.hash).then(done => {
        resolve(true);
      }).catch(err => reject(err));
    });

  }
  edit() {
    this.popover.dismiss();
    this.router.navigate(['/goal-edit/', this.goal.hash]);
  }
  ngOnInit() { }

}
