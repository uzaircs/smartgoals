import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HabitItem, HabitHistory } from 'src/app/models/habits/habit-item';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { Router } from '@angular/router';
import { DingSoundService } from 'src/app/utils/ding-sound.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-habit-actions-popover',
  templateUrl: './habit-actions-popover.component.html',
  styleUrls: ['./habit-actions-popover.component.scss'],
})
export class HabitActionsPopoverComponent implements OnInit {
  selectedAction: BehaviorSubject<number> = new BehaviorSubject(0);
  item: HabitItem;
  constructor(
    private db: GlobalStorageService,
    private router: Router,
    private ding: DingSoundService,
    private popover: PopoverController) { }
  optionSelected(option: number) {
    this.done();
    this.selectedAction.next(option);
  }
  start() {
    this.router.navigateByUrl('simple-count-down');
  }
  view() {
    this.popover.dismiss().then(done => {
      this.router.navigate(['/habit/', this.item.hash]);
    });
  }
  done() {
    const historyItem = new HabitHistory();
    historyItem.date = new Date();
    historyItem.isCompleted = true;
    historyItem.value = 1;
    this.db.addHabitHistory(this.item.hash, historyItem).then(done => {
      this.ding.play();
    });
  }
  ngOnInit() { }

}
