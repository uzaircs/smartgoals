import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoalMenuPopoverComponent } from '../goal-menu-popover/goal-menu-popover.component';
import { IonicModule } from '@ionic/angular';
import { SortPopoverComponent } from '../sort-popover/sort-popover.component';
import { HabitActionsPopoverComponent } from '../habit-actions-popover/habit-actions-popover.component';



@NgModule({
  declarations: [GoalMenuPopoverComponent, SortPopoverComponent, HabitActionsPopoverComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [GoalMenuPopoverComponent, SortPopoverComponent, HabitActionsPopoverComponent]
})
export class PopoverModule { }
