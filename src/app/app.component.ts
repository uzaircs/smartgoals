import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { GlobalStorageService } from './service/global-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private db: GlobalStorageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.db.dbReady.subscribe(ready => {
        setTimeout(() => {
          this.statusBar.styleDefault();
          this.splashScreen.hide();
          this.statusBar.overlaysWebView(false);
          this.statusBar.backgroundColorByHexString('#2dbcf3');
        }, 1000);
      });

    });
  }
}
