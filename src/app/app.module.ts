import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicSelectableModule } from 'ionic-selectable';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicGestureConfig } from 'src/utils/IonicGestureConfig';
import { Vibration } from '@ionic-native/vibration/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { ModalPageComponent } from './utils/modal-page/modal-page.component';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { PipesModule } from './pipes/pipes/pipes.module';
import { IonicStorageModule } from '@ionic/storage';
import { PopoverModule } from './popovers/popover/popover.module';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';


@NgModule({
  declarations: [AppComponent, ModalPageComponent],
  entryComponents: [],
  imports: [
    BrowserModule,

    IonicModule.forRoot({ mode: 'ios', animated: true }),
    IonicStorageModule.forRoot(
      {
        name: '__smartgoals',
        description: 'Smart Goals Local Database',
        driverOrder: ['sqlite', 'indexeddb', 'websql']
      }
    ),
    AppRoutingModule,
    IonicSelectableModule,
    HttpClientModule,
    PopoverModule,
    PipesModule],
  providers: [
    StatusBar,
    LocalNotifications,
    NativeAudio,
    SplashScreen,
    Vibration,
    NativePageTransitions,
    SQLite,
    SQLitePorter,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
