import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoiceFormPage } from './choice-form.page';

const routes: Routes = [
  {
    path: '',
    component: ChoiceFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChoiceFormPageRoutingModule {}
