import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoiceFormPageRoutingModule } from './choice-form-routing.module';

import { ChoiceFormPage } from './choice-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoiceFormPageRoutingModule
  ],
  declarations: [ChoiceFormPage]
})
export class ChoiceFormPageModule {}
