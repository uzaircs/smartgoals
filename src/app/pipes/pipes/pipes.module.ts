import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FromNowPipe } from '../from-now.pipe';
import { IonicModule } from '@ionic/angular';
import { PriorityDisplayPipe } from '../priority-display.pipe';
import { RepeatDisplayPipe } from '../repeat-display.pipe';
import { TaskCompletedPipe } from '../task-completed.pipe';
import { DecimalFormatPipe } from '../decimal-format.pipe';
import { ReadableConditionPipe } from '../readable-condition.pipe';
import { ReadableDatePipe } from '../readable-date.pipe';



@NgModule({
  declarations: [
    FromNowPipe,
    PriorityDisplayPipe,
    RepeatDisplayPipe,
    TaskCompletedPipe,
    DecimalFormatPipe,
    ReadableConditionPipe,
    ReadableDatePipe
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    FromNowPipe,
    PriorityDisplayPipe,
    RepeatDisplayPipe,
    TaskCompletedPipe,
    DecimalFormatPipe,
    ReadableConditionPipe,
    ReadableDatePipe
  ]
})
export class PipesModule { }
