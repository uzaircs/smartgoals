import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalFormat'
})
export class DecimalFormatPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value && !isNaN(value)) {
      if ((value % 1) > 0) {
        return parseFloat(value).toFixed(1);
      } else {
        return parseFloat(value).toFixed(0);
      }
    } else { return 0; }
  }

}
