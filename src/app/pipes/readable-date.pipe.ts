import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'readableDate'
})
export class ReadableDatePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value && moment(value).isValid()) {
      if (args[0]) {
        return moment(value).format('MMM D @ HH:ma');
      } else {
        return moment(value).format('Do MMM,YYYY');
      }
    }
  }

}
