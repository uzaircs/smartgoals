import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../service/database.service';

@Pipe({
  name: 'taskCompleted'
})
export class TaskCompletedPipe implements PipeTransform {

  transform(value: Task[], ...args: any[]): any {
    let noInverse = true;
    if (args && args[0] === false) {
      noInverse = false;
    }
    if (value && value.length) {
      return value.filter(task => noInverse ? task.status === 2 : task.status !== 2);
    }
  }

}
