import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priorityDisplay'
})
export class PriorityDisplayPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case 1:
        return 'Medium';
      case 2:
        return 'High';
      case 0:
        return 'Low';
      default:
        break;
    }
  }

}
