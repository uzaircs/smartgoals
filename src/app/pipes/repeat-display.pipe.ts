import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'repeatDisplay'
})
export class RepeatDisplayPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case 0:
        return 'No Repeat';
      case 1:
        return 'Repeat Daily';
      case 2:
        return 'Repeat every 2 days';

      default:
        break;
    }
  }

}
