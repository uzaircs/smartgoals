import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoaleditPageRoutingModule } from './goaledit-routing.module';

import { GoaleditPage } from './goaledit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoaleditPageRoutingModule
  ],
  declarations: [GoaleditPage]
})
export class GoaleditPageModule {}
