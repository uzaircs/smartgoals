import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoaleditPage } from './goaledit.page';

const routes: Routes = [
  {
    path: '',
    component: GoaleditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoaleditPageRoutingModule {}
