import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoaleditPage } from './goaledit.page';

describe('GoaleditPage', () => {
  let component: GoaleditPage;
  let fixture: ComponentFixture<GoaleditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoaleditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoaleditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
