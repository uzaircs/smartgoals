import { Component, OnInit } from '@angular/core';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { Goal } from 'src/app/service/database.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-goaledit',
  templateUrl: './goaledit.page.html',
  styleUrls: ['./goaledit.page.scss'],
})
export class GoaleditPage implements OnInit {
  goal: Goal;
  ready = false;
  saving = false;
  $goal = {
    name: true,
    end_date: true
  }
  private getHash(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.params.params.subscribe(params => {
        const hash = params.hash;
        if (hash) {
          resolve(hash);
        } else {
          reject('Hash not found');
        }
      });
    });
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {

      if (!this.goal.name) {
        this.$goal.name = false;
        reject('Goal Name is required');
      } else {
        this.$goal.name = true;
      }
      if (!this.goal.end_date) {
        this.$goal.end_date = false;
        reject('Due date is required');
      } else {
        this.$goal.end_date = true;
      }
      if (this.$goal.name && this.$goal.end_date) {
        resolve(true);
      }
    });

  }
  constructor(
    private db: GlobalStorageService,
    private params: ActivatedRoute,
    private nav: NavController,
    private toastr: ToastController
  ) { }
  async showToast() {
    const toast = await this.toastr.create({
      message: 'Goal edited succesfuly',
      header: 'Saved',
      duration: 2000,
      color: 'tertiary'
    });
    toast.present();
  }
  async showErrorToast(message: string) {
    const toast = await this.toastr.create({
      message,
      duration: 2000,
      header: 'Please check again',
      color: 'danger'
    });
    toast.present();
  }
  save() {
    this.validate().then(done => {
      this.saving = true;
      this.db.updateGoal(this.goal.hash, this.goal).then(done => {
        this.nav.navigateBack(['/goal-view/', this.goal.hash]);
        this.showToast();
        this.saving = false;
      });
    }).catch(err => {
      this.showErrorToast(err);
    });
  }
  load() {
    setTimeout(() => {
      this.defferedLoad().then(done => {
        this.ready = true;
      }).catch(err => this.nav.navigateRoot(['']));
    }, 400);
  }
  defferedLoad(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.getHash().then(hash => {
        this.db.getGoal(hash).then(goal => {
          this.goal = goal;
          console.log(goal);
          resolve(true);
        }).catch(err => reject(err));
      });
    });
  }
  ionViewDidEnter(): void {
    this.load();
  }
  ngOnInit() {
    this.load();
  }

}
