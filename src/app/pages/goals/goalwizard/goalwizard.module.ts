import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoalwizardPageRoutingModule } from './goalwizard-routing.module';

import { GoalwizardPage } from './goalwizard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoalwizardPageRoutingModule
  ],
  declarations: [GoalwizardPage]
})
export class GoalwizardPageModule {}
