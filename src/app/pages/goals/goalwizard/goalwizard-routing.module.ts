import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoalwizardPage } from './goalwizard.page';

const routes: Routes = [
  {
    path: '',
    component: GoalwizardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoalwizardPageRoutingModule {}
