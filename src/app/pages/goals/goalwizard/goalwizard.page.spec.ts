import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoalwizardPage } from './goalwizard.page';

describe('GoalwizardPage', () => {
  let component: GoalwizardPage;
  let fixture: ComponentFixture<GoalwizardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalwizardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoalwizardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
