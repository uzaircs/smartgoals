import { AutoUnique } from 'src/app/models/habits/habit-item';
import { Subscription } from 'rxjs';


export enum TimerType {
    GOAL = 1,
    TASK,
    HABIT
}
export interface ITimerBase {
    timeElapsed: number;
    title: string;
    timeRequired: number;
    startedOn: Date;
    endedOn: Date;
    timerType: TimerType;
    isRunning: boolean;
    interval: number;
    timer: Subscription;


}
export class TimerItem extends AutoUnique implements ITimerBase {
    timer: Subscription;
    interval: number;
    timeElapsed: number; title: string;
    timeRequired: number;
    startedOn: Date;
    endedOn: Date;
    timerType: TimerType;
    isRunning: boolean;


}
export interface ITimer extends ITimerBase {
    onTick();
    onPause();
    onStopped();
    toggle();
    stop();
    save(): ITimerBase;
}


