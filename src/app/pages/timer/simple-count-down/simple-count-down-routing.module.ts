import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimpleCountDownPage } from './simple-count-down.page';
import { CountDownTimerComponent } from '../count-down-timer/count-down-timer.component';

const routes: Routes = [
  {
    path: '',
    component: SimpleCountDownPage,
    children: [
      {
        path: '',
        redirectTo: 'timer'
      },
      {
        path: 'task/:hash',
        component: CountDownTimerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SimpleCountDownPageRoutingModule { }
