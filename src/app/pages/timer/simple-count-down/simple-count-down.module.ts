import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SimpleCountDownPageRoutingModule } from './simple-count-down-routing.module';

import { SimpleCountDownPage } from './simple-count-down.page';
import { CountDownTimerComponent } from '../count-down-timer/count-down-timer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SimpleCountDownPageRoutingModule
  ],
  declarations: [SimpleCountDownPage, CountDownTimerComponent]
})
export class SimpleCountDownPageModule { }
