import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SimpleCountDownPage } from './simple-count-down.page';

describe('SimpleCountDownPage', () => {
  let component: SimpleCountDownPage;
  let fixture: ComponentFixture<SimpleCountDownPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleCountDownPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SimpleCountDownPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
