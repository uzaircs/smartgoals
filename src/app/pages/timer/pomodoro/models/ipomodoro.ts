import { Observable, Subscription, interval, BehaviorSubject } from 'rxjs';
import { AutoUnique } from 'src/app/models/habits/habit-item';
import { Task, Goal } from 'src/app/service/database.service';

export class TimeElapsed {
    countdown = false;
    seconds: number;
    minutes: number;
    hours: number;
    days: number;
    startedAt: Date;
    endedAt: Date;
    elapsed = 0;
    private $tick = new BehaviorSubject<number>(0);
    private $$tick = this.$tick.asObservable();
    private $done = new BehaviorSubject<boolean>(false);

    tick() {
        if (!this.countdown && !this.$done.getValue()) {
            this.seconds++;
            this.elapsed++;
            this.normalizeSeconds();
            this.$tick.next(this.elapsed);
        } else {
            this.untick();
        }
    }
    untick() {
        this.seconds--;
        this.elapsed--;
        this.$tick.next(this.elapsed);
        if (this.checkFinish()) {
            console.log('finished');
            this.$done.next(true);
        }
        this.normalizeSeconds();

    }
    timeOver(): Promise<TimeElapsed> {
        return new Promise((resolve, reject) => {
            if (!this.countdown) {
                reject('Not a countdown timer');
            }
            this.$done.subscribe(t => {
                if (t) {
                    resolve(this);
                }
            });
        });
    }
    private checkFinish(): boolean {
        return this.countdown && this.seconds <= 0 && this.minutes <= 0 && this.hours <= 0;
    }
    tickWatcher(): Observable<number> {
        return this.$$tick;
    }
    private onFinish(callback) {
        if (callback) {
            callback(this.elapsed);
        }
    }
    private normalizeSeconds() {
        if (this.seconds >= 60) {
            this.seconds = 0;
            this.minutes++;
            this.normalizeMinutes();
        }
        if (this.countdown && this.seconds <= 0) {
            this.seconds = 60;
            this.minutes--;
            this.normalizeMinutes();
        }
    }
    private normalizeMinutes() {
        if (this.minutes >= 60) {
            this.minutes = 0;
            this.hours++;
            this.normalizeHours();
        }
        if (this.countdown && this.minutes <= 0) {
            this.minutes = 60;
            this.hours--;
            this.normalizeHours();
        }
    }
    private normalizeHours() {
        if (this.hours >= 24) {
            this.hours = 0;
            this.days++;
        }
        if (this.countdown && this.hours <= 0) {
            this.hours = 0;
            this.days--;
        }
    }
    /**
     * @description
     * Initializes a new instance of Pomodoro Timer
     * @param config Default settings for this timer `{seconds,minutes,hours}`
     * @param countdown Specifies wether the timer is countdown or stopwatch mode
     */
    constructor(config: TimeElapsed = null, countdown = false) {
        if (config) {
            this.seconds = config.seconds || 0;
            this.minutes = config.minutes || 0;
            this.hours = config.hours || 0;
            this.days = config.days || 0;
            this.countdown = countdown;
        } else {
            this.seconds = 0;
            this.hours = 0;
            this.minutes = 0;
            this.days = 0;
        }
    }
}
export interface IPomodoro {
    elapsed: TimeElapsed;
    paused: boolean;
    required: TimeElapsed;
    passed: boolean;
    start(): Observable<TimeElapsed>;
    toggle(): boolean;
    stop(): Promise<TimeElapsed>;
}
export class Pomodoro extends AutoUnique implements IPomodoro {
    elapsed: TimeElapsed;
    private $elapsed: BehaviorSubject<TimeElapsed> = new BehaviorSubject(new TimeElapsed());
    private $$elapsed = this.$elapsed.asObservable();
    private $onStop = new BehaviorSubject<TimeElapsed>(null);
    private $$onStop = this.$onStop.asObservable();
    paused = false;
    started = false;
    passed = false;
    required: TimeElapsed;
    interval: Subscription;
    isRunning(): boolean {
        return this.started && !this.paused;
    }
    start(): Observable<TimeElapsed> {
        if (!this.started) {
            this.interval = interval(1000).subscribe(tick => this.onTick());
            this.started = true;
            this.elapsed.startedAt = new Date();
            this.elapsed.timeOver().then(done => {
                if (this.interval) {
                    this.elapsed.endedAt = new Date();
                    this.interval.unsubscribe();
                    this.$onStop.next(this.elapsed);
                }
            });
        }
        return this.$$elapsed;
    }
    private onTick() {
        if (!this.paused) {
            this.elapsed.tick();

            this.$elapsed.next(this.elapsed);
        }
    }
    stop(): Promise<TimeElapsed> {
        return new Promise((resolve, reject) => {
            if (this.interval) {
                this.interval.unsubscribe();
                resolve(this.elapsed);
            } else {
                reject('Timer was never started');
            }
        });

    }
    onStop(): Observable<TimeElapsed> {
        return this.$$onStop;

    }
    toggle(): boolean {
        this.paused = !this.paused;
        return this.paused;
    }
    constructor(required: TimeElapsed = null) {
        super();
        if (required) {
            this.elapsed = required;
        } else {
            this.elapsed = new TimeElapsed();
        }
    }

}
export class TaskPomodoro extends Pomodoro {
    task: Task;
    constructor(task: Task, config: TimeElapsed = null) {
        super(config);
        if (!task) {
            throw Error('No task defined for timer');
        } else {
            this.task = task;
        }

    }
}
export class GoalPomodoro extends Pomodoro {
    goal: Goal;
    constructor(goal: Goal) {
        super();
        if (!goal) {
            throw Error('No goal defined for timer');
        } else {
            this.goal = goal;
        }

    }
}


