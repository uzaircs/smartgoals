import { Injectable } from '@angular/core';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { Pomodoro, IPomodoro, TaskPomodoro } from '../models/ipomodoro';

@Injectable({
  providedIn: 'root'
})
export class TimerManagerService {
  timer: Pomodoro;
  constructor(private db: GlobalStorageService) { }
  save(passed = false): Promise<Pomodoro> {
    return new Promise((resolve, reject) => {
      if (passed) {
        this.timer.passed = passed;
      }
      this.db.saveTimer(this.timer).then(saved => {
        resolve(saved);
      }).catch(err => reject(err));
    });
  }
  dispose(): Promise<any> {
    return new Promise((resolve, reject) => {
      reject('Not implemented');
    });

  }
  set(timer: Pomodoro) {
    this.timer = timer;
  }
  get(): IPomodoro {
    return this.timer;
  }
  hasTimer(): boolean {
    return (this.timer !== null && this.timer !== undefined);
  }
}
