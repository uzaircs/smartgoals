import { Component, OnInit } from '@angular/core';
import { ITimer, TimerType } from '../Models/itimer-model';
import { interval, Subscription, VirtualTimeScheduler } from 'rxjs';
import { TaskPomodoro, TimeElapsed, Pomodoro } from '../pomodoro/models/ipomodoro';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { ActivatedRoute } from '@angular/router';
export class TimeSpan {
  minutes = 0;
  seconds = 0;
  hours = 0;

  parseFromSeconds(seconds: number) {
    this.minutes = parseInt((seconds / 60).toFixed(0), 10);
    this.seconds = seconds - (this.minutes * 60);

  }
}
@Component({
  selector: 'app-count-down-timer',
  templateUrl: './count-down-timer.component.html',
  styleUrls: ['./count-down-timer.component.scss'],
})
export class CountDownTimerComponent implements OnInit, ITimer {
  interval = 1;
  timer: Subscription;
  timeElapsed = 0;
  title: string;
  timeRequired = 3000;
  startedOn: Date;
  endedOn: Date;
  timerType: TimerType;
  isRunning: boolean;
  started = false;
  elapsed: TimeSpan = new TimeSpan();
  $$elapsed: TimeElapsed;

  pomodoro: TaskPomodoro;
  onTick() {
    this.timeElapsed += this.interval;
    this.elapsed.parseFromSeconds(this.timeElapsed);

  }
  onPause() {

  }
  onStopped() {
    this.started = false;
  }
  stop() {
    if (this.started) {
      this.timer.unsubscribe();
    }

  }

  save(): import("../Models/itimer-model").ITimerBase {
    throw new Error("Method not implemented.");
  }

  constructor(private db: GlobalStorageService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      const hash = params.hash;
      this.db.getTasks().subscribe(tasks => {
        const task = tasks.find(t => t.hash === hash);
        if (task) {
          this.pomodoro = new TaskPomodoro(task);
          this.pomodoro.elapsed = new TimeElapsed();
          this.pomodoro.elapsed.minutes = 25;
          this.pomodoro.elapsed.countdown = true;
          this.pomodoro.start();
          this.started = true;
          this.pomodoro.started = true;
          this.$$elapsed = this.pomodoro.elapsed;
          this.pomodoro.onStop().subscribe(stopped => {
            this.onStopped();
          });
        }
      });
    });

  }
  toggle() {

    if (!this.pomodoro.started) {
      this.pomodoro.start().subscribe(elapsed => {
        this.$$elapsed = elapsed;
      });

    } else {
      this.pomodoro.toggle();
      this.started = false;
    }
  }
  ngOnInit() { }

}
