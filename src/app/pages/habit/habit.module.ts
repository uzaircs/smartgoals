import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HabitPageRoutingModule } from './habit-routing.module';

import { HabitPage } from './habit.page';
import { HabitHistoryViewComponent } from './habit-history-view/habit-history-view.component';
import { PipesModule } from 'src/app/pipes/pipes/pipes.module';

import { UIComponentsModule } from 'src/app/uicomponents/uicomponents.module';
import { RevisionComponent } from './revision/revision.component';
import { BreakdownComponent } from './breakdown/breakdown.component';
import { CalendarViewPage } from 'src/app/uicomponents/calendar/calendar-view/calendar-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HabitPageRoutingModule,
    PipesModule,
    UIComponentsModule,
    

  ],
  declarations: [HabitPage, HabitHistoryViewComponent, RevisionComponent, BreakdownComponent]
})
export class HabitPageModule { }
