import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CheckResultItem } from '../../habit-wizard/habit-container/validator.service';
import { CalendarManagerService } from 'src/app/uicomponents/calendar/calendar-manager.service';
import { CalendarItem } from 'src/app/uicomponents/calendar/calendar-item';
import * as moment from 'moment';
import { HabitItem } from 'src/app/models/habits/habit-item';


@Component({
  selector: 'app-habit-history-view',
  templateUrl: './habit-history-view.component.html',
  styleUrls: ['./habit-history-view.component.scss'],
})
export class HabitHistoryViewComponent implements OnInit {
  @Input() history: CheckResultItem[];
  @Input() habit: HabitItem;
  @Output() daySelected: EventEmitter<CalendarItem> = new EventEmitter();
  constructor(private calendarService: CalendarManagerService) { }

  days: CalendarItem[] = [];
  changed($event: CalendarItem) {
    this.daySelected.emit($event);
  }
  ngOnInit() {
    this.days = this.calendarService.prepare(moment().get('month'), moment().get('year'));
    this.days.forEach(day => {
      if (day.date < moment(this.habit.startDate).toDate()) {
        day.isRelevant = false;
      } else if (day.date >= moment().toDate()) {
        day.isRelevant = false;
      }
    })
    this.history.forEach(item => {
      const month = item.date.getMonth();

      if (month === moment().get('month')) {
        const index = this.days.findIndex(d => d.date.getMonth() === month && d.date.getDate() === item.date.getDate());
        if (index >= 0) {
          this.days[index].isCompleted = item.storedValue;
        }
      }
    });
  }

}
