import { Component, OnInit, Input } from '@angular/core';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { Subscription } from 'rxjs';
import { CheckResultItem, ValidatorService, TimePeriodItem } from '../../habit-wizard/habit-container/validator.service';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { ActivatedRoute } from '@angular/router';
import { StatisticsService, BreakdownItem } from 'src/app/service/statistics.service';
import { NavController } from '@ionic/angular';
import { CalendarItem } from 'src/app/uicomponents/calendar/calendar-item';

@Component({
  selector: 'app-breakdown',
  templateUrl: './breakdown.component.html',
  styleUrls: ['./breakdown.component.scss'],
})
export class BreakdownComponent implements OnInit {
  habitItem: HabitItem;
  subscription: Subscription;
  ready = false;
  condition = 'None';
  results: CheckResultItem[];
  breakdown: BreakdownItem;
  @Input() hash: string;
  constructor(
    private db: GlobalStorageService,
    private activatedRoute: ActivatedRoute,
    private validator: ValidatorService,
    private statService: StatisticsService,
    private nav: NavController
  ) { }
  defferedLoad(hash): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (hash) {
        this.subscription = this.db.getHabits().subscribe(habits => {
          if (habits && habits.length > 0) {
            const index = habits.findIndex(h => h.hash === hash);
            if (index >= 0) {
              this.habitItem = habits[index];

              this.results = this.validator.check(this.habitItem);
              const item = new TimePeriodItem(this.habitItem);
              this.condition = item.toString();
              resolve(true);
            }
          }
        });
      }
    });
  }
  onDaySelect($event: CalendarItem) {
    if ($event.isRelevant) {
      this.breakdown = this.statService.getHabitBreakDown(this.habitItem, $event.date);
    }

  }
  ngOnInit() {
    if (this.hash) {

      this.defferedLoad(this.hash).then(done => {
        if (done) {
          this.ready = true;
        }
      });
    }
  }

}
