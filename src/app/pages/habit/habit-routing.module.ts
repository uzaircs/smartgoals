import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HabitPage } from './habit.page';
import { RevisionComponent } from './revision/revision.component';

const routes: Routes = [
  {
    path: '',
    component: HabitPage
  },
  {
    path: '/edit/:hash',
    component: RevisionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HabitPageRoutingModule { }
