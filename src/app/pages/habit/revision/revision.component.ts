import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-revision',
  templateUrl: './revision.component.html',
  styleUrls: ['./revision.component.scss'],
})
export class RevisionComponent implements OnInit {
  hash: string;
  habitItem: HabitItem;
  constructor(
    private activatedRoute: ActivatedRoute,
    private db: GlobalStorageService,
    private toast: ToastController
  ) {
    this.activatedRoute.params.subscribe(p => {
      this.hash = p.hash;

      if (this.hash) {
        this.db.getHabits().subscribe(h => {
          const index = h.findIndex(habit => habit.hash === this.hash);
          if (index >= 0) {
            this.habitItem = h[index];

          } else {
            this.notFound();
          }
        });
      }
    });
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.habitItem.name) {
        reject('Name is a required field');
      }
    });
  }
  save() {

  }
  async errorToast(message: string) {
    const toast = await this.toast.create({
      message,
      header: 'Please check',
      duration: 2000,

    });
    toast.present();
  }
  notFound() {

  }
  ngOnInit() { }

}
