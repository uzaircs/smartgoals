import { Component, OnInit } from '@angular/core';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ValidatorService, CheckResultItem, PeriodFormatter, TimePeriodItem } from '../habit-wizard/habit-container/validator.service';
import { StatisticsService } from 'src/app/service/statistics.service';
import { NavController } from '@ionic/angular';
import { HabitContainerService } from '../habit-wizard/habit-container/habit-container.service';

@Component({
  selector: 'app-habit',
  templateUrl: './habit.page.html',
  styleUrls: ['./habit.page.scss'],
})
export class HabitPage implements OnInit {
  habitItem: HabitItem;
  subscription: Subscription;
  ready = false;
  condition = 'None';
  results: CheckResultItem[];
  constructor(
    private db: GlobalStorageService,
    private activatedRoute: ActivatedRoute,
    private validator: ValidatorService,
    private statService: StatisticsService,
    private nav: NavController,
    private container: HabitContainerService
  ) { }
  load() {
    this.defferedLoad().then(done => this.ready = true);
  }
  edit() {
    this.container.setItem(this.habitItem);

    this.container.isUpdating = true;
    this.nav.navigateForward('/habit-wizard/update/specific/' + this.habitItem.hash);

  }
  defferedLoad(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.activatedRoute.params.subscribe(params => {
          const hash = params.hash;
          if (hash) {
            this.subscription = this.db.getHabits().subscribe(habits => {
              if (habits && habits.length > 0) {
                const index = habits.findIndex(h => h.hash === hash);
                if (index >= 0) {
                  this.habitItem = habits[index];
                  this.results = this.validator.check(this.habitItem);
                  const item = new TimePeriodItem(this.habitItem);
                  this.condition = item.toString();
                  resolve(true);
                }
              }
            });
          }
        });
      }, 300);
    });
  }

  ngOnInit() {
    this.load();
  }
  ionViewDidLeave(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
