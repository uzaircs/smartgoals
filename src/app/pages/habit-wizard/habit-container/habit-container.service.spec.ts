import { TestBed } from '@angular/core/testing';

import { HabitContainerService } from './habit-container.service';

describe('HabitContainerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HabitContainerService = TestBed.get(HabitContainerService);
    expect(service).toBeTruthy();
  });
});
