import { Injectable } from '@angular/core';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { PolarityFormatter } from './validator.service';

@Injectable({
  providedIn: 'root'
})
export class TransformerService {

  constructor() { }
  transform(habit: HabitItem): string {
    if (!habit) {
      return 'Not Specified';
    } else {
      if (habit.repeatItem.timePeriod) {
        let readableString = '';
        const period = ['day', 'week', 'month', 'year'][habit.repeatItem.timePeriod - 1];
        readableString += habit.repeatItem.interval + ' ' + period;
        readableString += habit.repeatItem.interval > 1 ? 's' : '';
        readableString += habit.repeatItem.interval > 1 ? ' are ' : ' is';
        readableString += ' successful when ' + habit.name;
        readableString += habit.condition.isCompleted ? ' is completed ' : ' is avoided ';
        readableString += PolarityFormatter.polarityToString(habit.condition.polarity).toLowerCase() + ' ';
        readableString += habit.repeatItem.repetitions + ' times';
        return readableString;

      } else {
        return '';
      }
    }
  }
}

