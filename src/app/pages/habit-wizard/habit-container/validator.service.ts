import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { TimePeriod, RepeatItem } from 'src/app/models/repeat/repeat-item';
import { HistoryItem, HabitHistory, WinCondition, ConditionPolarity, HabitItem } from 'src/app/models/habits/habit-item';
export class PeriodFormatter {
  public static relative(period: TimePeriodItem): string {
    let relative = 'Current ';
    if (period.interval > 1) {
      relative += period.interval + ' ';
      relative += this.periodToString(period.period);
    }
    if (period.interval === 1) {
      if (period.period === TimePeriod.DAY) {
        relative = 'Today';
      }
      if (period.period === TimePeriod.WEEK) {
        relative = 'This Week';
      }

      if (period.period === TimePeriod.MONTH) {
        relative = 'This Month';
      }
      if (period.period === TimePeriod.YEAR) {
        relative = 'This Year';
      }
    }


    return relative;

  }
  public static periodToString(period: TimePeriod) {
    switch (period) {
      case TimePeriod.DAY:
        return 'days';
      case TimePeriod.MONTH:
        return 'months';
      case TimePeriod.WEEK:
        return 'weeks';
      case TimePeriod.YEAR:
        return 'years';
      default:
        break;
    }
  }
}
/**
 * Format the polarity to string
 */
export class PolarityFormatter {
  public static polarityToString(polarity: ConditionPolarity): string {
    switch (polarity) {
      case ConditionPolarity.ATLEAST:
        return 'Atleast';

      case ConditionPolarity.EXACTLY:
        return 'Exactly';

      case ConditionPolarity.NOT_MORE_THAN:
        return 'Not More than';

      default:
        return '-';
    }
  }
}
export enum CheckResult {
  COMPLETED = 1,
  FAILED,
  INSUFFICIENT,
  NOT_STARTED
}
export class TimePeriodItem {
  period: TimePeriod;
  interval: number;
  requiredValue: number;
  isCompleted = true;
  polarity: ConditionPolarity;
  toRelativeFormat(): string {
    return PeriodFormatter.relative(this);
  }
  /**
   *
   */
  constructor(habit: HabitItem = null) {

    if (habit) {
      this.period = habit.repeatItem.timePeriod;
      this.interval = habit.repeatItem.interval;
      this.isCompleted = habit.condition.isCompleted;
      this.requiredValue = habit.condition.value;
      this.polarity = habit.condition.polarity;
    }
  }
  toReadableFormat(normalizePlurals = true): string {
    let str = PeriodFormatter.periodToString(this.period);
    if (this.interval === 1 && normalizePlurals) {
      str = str.substr(0, str.length - 1);
    }
    if (this.interval > 1) {
      str = this.interval + ' ' + str;
    }
    return str;
  }
  valueFormatter(value: number) {
    switch (value) {
      case 1:
        return 'once';
      case 2:
        return 'twice';
      case 3:
        return 'thrice';
      default:
        return value + ' times';
    }
  }
  toString(): string {
    return PolarityFormatter
      .polarityToString(this.polarity) + ' ' + this.valueFormatter(this.requiredValue) + ' per ' + this.toReadableFormat();
  }

}

export class HabitSummaryItem {
  percentage: number;
  remaining: number;
  completed: number;
  isLimited: boolean;
  condition: WinCondition;
  period: TimePeriodItem;
  periodLength: number;
}
export class CheckResultItem {
  date: Date;
  verified = false;
  result: CheckResult;
  requiredValue: number;
  storedValue: number;
}
@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  DATE_FORMAT = 'DDMMYYYY';
  constructor() { }
  /**
   * @description
   * Generates a sequence of dates with a date as key and its value being an array of HistoryItem of a habit
   * Example input start = 01/01/2020, repeat = {interval = 1,timePeriod = TimePeriod.DAY,repetitions = 2}
   * @returns A sequence object as JSON
   * @param start Start date for this sequence
   * @param repeat The RepeatItem object that contains *interval*,*timePeriod*,*repetitions* for a HabitItem
   * @param endDate The end boundary of this sequence
   */
  generateSequence(start: any, repeat: RepeatItem, endDate: any = new Date()) {
    if (this.validateDate(start) && this.validateRepeatItem(repeat)) {

      const sequence = {};
      sequence[moment(start).format(this.DATE_FORMAT)] = [];
      let n: any = moment(start);
      /**
       * Moment a - b -> returns difference in 'days'
       * Example moment(2/1/2020).diff(1/1/2020) = 1
       */

      while (moment(n).diff(moment(endDate), 'days') < 0) {
        n = moment(n).add(repeat.interval, this.periodToString(repeat.timePeriod));

        sequence[moment(n).format(this.DATE_FORMAT)] = [];
      }
      return sequence;
    }
  }
  /**
   * @description
   * Map Habit history to a sequence for validating conditions
   * @param sequence The sequence object containing dates entries generated from generateSequence e.g sequence = { 112020 :[],122020 :[] }
   * @param items The history items for a habit
   * @returns Modified sequence object mapped with history items
   */
  mapSequence(sequence: any, items: HabitHistory[]) {
    if (!items || !items.length) {
      return sequence;
    }
    items.forEach(item => {
      if (this.validateDate(item.date)) {
        const parsedDate = moment(item.date).format(this.DATE_FORMAT);

        if (sequence[parsedDate] && Array.isArray(sequence[parsedDate])) {
          sequence[parsedDate].push(item.isCompleted);
        } else {

          const key = this.findMedianKey(sequence, item.date);

          if (sequence[key] && Array.isArray(sequence[key])) {
            sequence[key].push(item.isCompleted);
          }
        }
      }
    });
    return sequence;
  }
  findMedianKey(sequence: any, date: Date) {
    let key = '';
    if (this.validateDate(date)) {
      const sequenceDates = Object.keys(sequence);

      sequenceDates.forEach((d, i, array) => {

        if (sequence.hasOwnProperty(d)) {

          const dateFirst = this.parseKey(d);
          if (array[i + 1]) {

            const dateSecond = this.parseKey(array[i + 1]);

            if (moment(date).isBefore(moment(dateSecond)) && moment(date).isAfter(moment(dateFirst))) {

              key = moment(dateSecond).format(this.DATE_FORMAT);

            }
          } else {
            key = moment(dateFirst).format(this.DATE_FORMAT);
          }
        }
      });
    }
    return key;

  }
  resultsPadding(items: CheckResultItem[], minPadding: number = 7): CheckResultItem[] {
    let sequence = this.sortSequence(items);
    while (items.length < minPadding) {
      sequence = this.sortSequence(items);
      const resultItem = new CheckResultItem();
      resultItem.date = moment(items[0].date).subtract(1, 'days').toDate();
      resultItem.verified = false;
      resultItem.result = CheckResult.NOT_STARTED;
      sequence.push(resultItem);

    }
    return sequence;
  }
  sortSequence(items: CheckResultItem[]) {
    return items.sort((a, b) => a.date.getTime() - b.date.getTime());
  }
  private sortSequenceObject(items: any): any {
    const keys = Object.keys(items).filter(key => items.hasOwnProperty(key));
    const sortedKeys = keys.sort((a, b) => this.parseKey(a).getTime() - this.parseKey(b).getTime());
    const sequence = {};
    sortedKeys.forEach(key => {
      sequence[key] = items[key];
    });
    return sequence;
  }
  private getLastKey(sequence: any): any {
    const keys = Object.keys(sequence).filter(key => sequence.hasOwnProperty(key));
    const sortedKeys = keys.sort((a, b) => this.parseKey(a).getTime() - this.parseKey(b).getTime());
    return sortedKeys[sortedKeys.length - 1];
  }
  /**
   * @description
   * Gets a short summary of an active habit item
   * @param habit The habit item to get summary of
   * @returns HabitSummaryItem
   */
  getSummary(habit: HabitItem): HabitSummaryItem {
    const item = new HabitSummaryItem();
    let pendingItem = this.mapSequence(this.generateSequence(habit.startDate, habit.repeatItem), habit.history);
    if (pendingItem) {
      const lastKey = this.getLastKey(pendingItem);
      pendingItem = pendingItem[lastKey];
      const storedValue = this.getStoredValue(pendingItem, habit.condition.isCompleted);
      item.completed = storedValue;
      if (storedValue > 0) {
        item.percentage = parseInt(((storedValue / habit.condition.value) * 100).toFixed(0), 10);

      } else {
        item.percentage = 0;
      }
      item.remaining = (habit.condition.value - storedValue);
    }

    item.condition = habit.condition;

    item.isLimited = (habit.condition.polarity === ConditionPolarity.NOT_MORE_THAN);
    item.period = new TimePeriodItem();
    item.period.interval = habit.repeatItem.interval;
    item.period.isCompleted = habit.condition.isCompleted;
    item.period.requiredValue = habit.condition.value;
    item.period.polarity = habit.condition.polarity;
    item.period.period = habit.repeatItem.timePeriod;

    return item;
  }
  periodToString(period: TimePeriod): moment.unitOfTime.DurationConstructor {
    return PeriodFormatter.periodToString(period);
  }
  /**
   * @description
   * Match the habit with its input from user to determine wether the habit was succeded of failed
   * @returns boolean true/false true if condition matches the user input and false if condition fails to match
   * @param storedValue The stored value obtained from getStoredValue function of Sequence Item
   * @param condition The *Win condition* of the habit item
   */
  private match(storedValue: number, condition: WinCondition): boolean {
    if (isNaN(condition.value)) {
      condition.value = 1;
    }
    if (condition.polarity === ConditionPolarity.EXACTLY) {
      return condition.value === storedValue;
    }
    if (condition.polarity === ConditionPolarity.ATLEAST) {
      return storedValue >= condition.value;
    }
    if (condition.polarity === ConditionPolarity.NOT_MORE_THAN) {
      return storedValue > 0 && storedValue <= condition.value;

    }
    return false;
  }
  /**
   * @description
   * Get Result as **CheckResult** to determine wether a habit with specified **storedValue** was failed,completed, or insufficient
   * @param storedValue Stored entries from user in HabitHistory item generated from Sequence
   * @param condition The Wincondition of the habit item
   * @returns CheckResult The calculated result e.g **CheckResult.COMPLETED** if conditions matches
   */
  getResult(storedValue: number, condition: WinCondition): CheckResult {
    const result = this.match(storedValue, condition);
    if (result) {
      return CheckResult.COMPLETED;
    } else {
      if ((condition.polarity === ConditionPolarity.EXACTLY || condition.polarity === ConditionPolarity.ATLEAST)
        && storedValue < condition.value && storedValue > 0) {
        return CheckResult.INSUFFICIENT;
      }
    }

    return CheckResult.FAILED;
  }
  /**
   * @description
   * Converts formatted date to orignal date.
   * Keys are used for fast index accessing in Sequence array to avoid loops
   * @example parseKey('02022020'); -> returns 02/02/2020 as Date Object 
   * @returns Date
   * @param key Formatted date string
   * @uses DATE_FORMAT by default the key property uses MMDDYYYY
   */
  parseKey(key: string): Date {
    return new Date(parseInt(key.substr(4, 4), 10), parseInt(key.substr(2, 2), 10) - 1, parseInt(key.substr(0, 2), 10));
  }
  check(item: HabitItem): CheckResultItem[] {

    const results: CheckResultItem[] = [];
    const sequence = this.mapSequence(this.generateSequence(item.startDate, item.repeatItem), item.history);
    if (!sequence) {
      return [];
    }
    const keys = Object.keys(sequence);

    if (keys) {
      keys.forEach(key => {
        if (sequence.hasOwnProperty(key)) {
          const resultItem = new CheckResultItem();
          const storedValue = this.getStoredValue(sequence[key], item.condition.isCompleted);
          resultItem.verified = this.match(storedValue, item.condition);
          resultItem.date = this.parseKey(key);
          resultItem.result = this.getResult(storedValue, item.condition);
          resultItem.requiredValue = item.condition.value;
          resultItem.storedValue = storedValue;
          if (resultItem.result === CheckResult.FAILED && moment(resultItem.date).isSame(new Date(), 'day')) {
            resultItem.result = CheckResult.INSUFFICIENT;
          }
          results.push(resultItem);
        }
      });
    }
    return this.sortSequence(this.resultsPadding(results));

  }
  /**
   * @description
   * Get stored value i.e number of success for a single timeframe for a habit item
   * @param sequenceItem A sequence item entry from Sequence Item object Generated from generateSequence
   * @param isCompleted From WinCondition of the habit item to calculate number of isCompleted from sequenceItem
   */
  getStoredValue(sequenceItem: Array<boolean>, isCompleted: boolean): number {
    let value = 0;
    sequenceItem.forEach(item => {
      value += (item === isCompleted) ? 1 : 0;
    });
    return value;
  }
  validateInterval(interval: any): boolean {
    return interval && !isNaN(interval) && interval > 0;
  }
  validateHistoryItem(item: HistoryItem) {
    return item && item.date && this.validateDate(item.date);
  }
  validateRepeatItem(item: RepeatItem) {
    return item
      && this.validateInterval(item.interval) && this.validateTimePeriod(item.timePeriod)
      && this.validateInterval(item.repetitions);
  }
  parseTimePeriod(period: number): TimePeriod {
    return TimePeriod[TimePeriod[period]];
  }
  validateTimePeriod(period: any): boolean {
    return this.validateInterval(period);
  }
  validateDate(date): boolean {
    return date && moment(date).isValid() && moment(Date.parse(date)).isValid();
  }
}
