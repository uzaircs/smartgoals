import { Injectable } from '@angular/core';
import { HabitItem, WinCondition, ConditionPolarity } from 'src/app/models/habits/habit-item';
import { RepeatItem, TimePeriod } from 'src/app/models/repeat/repeat-item';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})
export class HabitContainerService {
  habit: HabitItem;
  isUpdating: boolean;
  constructor() {
    this.generateItem();
  }
  /**
   * Generate a new habit item to be used across the habit wizard
   * @returns HabitItem
   */
  generateItem(): HabitItem {
    this.habit = new HabitItem();
    this.habit.repeatItem = new RepeatItem();
    this.habit.repeatItem.interval = 1;
    this.habit.repeatItem.timePeriod = TimePeriod.DAY;
    this.habit.repeatItem.repetitions = 1;
    this.habit.condition = new WinCondition();
    this.habit.condition.isCompleted = true;
    this.habit.repetitions = 66;
    this.habit.condition.polarity = ConditionPolarity.ATLEAST;
    this.habit.condition.value = 1;
    this.habit.startDate = moment().format('YYYY-MM-DD');
    return this.habit;
  }
  getItem(): HabitItem {
    return this.habit;
  }
  resetItem() {
    this.generateItem();
  }
  setItem(habit: HabitItem) {
    this.habit = habit;
  }
}
