import { Component, OnInit } from '@angular/core';
import { HabitContainerService } from './habit-container/habit-container.service';

@Component({
  selector: 'app-habit-wizard',
  templateUrl: './habit-wizard.page.html',
  styleUrls: ['./habit-wizard.page.scss'],
})
export class HabitWizardPage implements OnInit {

  constructor(private habitContainer: HabitContainerService) {
    if (!this.habitContainer.isUpdating) {
      this.habitContainer.generateItem();
    }
  }

  ngOnInit() {
  }

}
