import { Component, OnInit } from '@angular/core';
import { HabitContainerService } from '../habit-container/habit-container.service';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { TransformerService } from '../habit-container/transformer.service';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-habit-review',
  templateUrl: './habit-review.component.html',
  styleUrls: ['./habit-review.component.scss'],
})
export class HabitReviewComponent implements OnInit {
  habit: HabitItem;
  condition: string;
  constructor(
    private container: HabitContainerService,
    private transform: TransformerService,
    private db: GlobalStorageService,
    private navCtrl: NavController,
    private toast: ToastController,
    private alert: AlertController
  ) { }
  async presentToast() {
    const toast = await this.toast.create({
      header: 'Habit Saved',
      message: this.habit.name + ' was saved succesfuly',
      duration: 2500,
      color: 'success'
    });
    toast.present();
  }
  async presentAlert(): Promise<any> {
    const alert = this.alert.create({
      header: 'Please confirm',
      message: 'Do you want to remove existing history for this habit',
      buttons: [{
        text: 'Keep History',
        handler: () => {
          this.alert.dismiss();
          this.save(true, false);
        },

      },
      {
        text: 'Delete History',
        role: 'destructive',
        handler: () => {
          this.save(true, true);
        }
      }
      ]
    });
    return (await alert).present();
  }

  /**
   * Save habit to local storage
   */
  save(confirmHistoryDelete, deleteHistory) {

    if (this.container.isUpdating) {
      if (!confirmHistoryDelete || typeof deleteHistory === undefined) {
        this.presentAlert();
        return false;
      }
      if (deleteHistory) {
        this.db.resetHabitHistory(this.habit);
        this.habit.startDate = moment().format('YYYY-MM-DD');
      }
      this.db.updateHabit(this.habit.hash, this.habit).then(done => {
        this.presentToast();
        this.navCtrl.navigateBack('/habit/' + this.habit.hash);

      });
    } else {
      this.db.addHabit(this.habit).then(done => {
        this.presentToast();
        this.navCtrl.navigateRoot('');

      });
    }

  }
  ngOnInit() {
    this.habit = this.container.getItem();
    this.condition = this.transform.transform(this.habit);
    console.log(this.habit);
  }

}
