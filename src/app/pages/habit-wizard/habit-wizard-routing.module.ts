import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HabitWizardPage } from './habit-wizard.page';
import { HabitCategoryComponent } from './habit-category/habit-category.component';
import { HabitTrackingComponent } from './habit-tracking/habit-tracking.component';
import { HabitPositiveTypicalComponent } from './habit-positive-typical/habit-positive-typical.component';
import { HabitStackingComponent } from './habit-stacking/habit-stacking.component';
import { HabitReviewComponent } from './habit-review/habit-review.component';
import { HabitSpecificComponent } from './habit-specific/habit-specific.component';
import { HabitBasicComponent } from './habit-basic/habit-basic.component';

const routes: Routes = [
  {
    path: '',
    component: HabitWizardPage,
    children: [
      {
        path: 'update/specific/:hash',
        component: HabitSpecificComponent
      },
      {
        path: 'update/basic/:hash',
        component: HabitPositiveTypicalComponent
      },
      {
        path: '',
        redirectTo: 'tracking',
        pathMatch: 'full',
      },
      {
        path: 'start',
        component: HabitCategoryComponent

      },
      {
        path: 'tracking',
        component: HabitTrackingComponent
      },
      {
        path: 'typical',
        component: HabitPositiveTypicalComponent
      },
      {
        path: 'stacking',
        component: HabitStackingComponent
      },
      {
        path: 'preview',
        component: HabitReviewComponent
      },
      {
        path: 'specific',
        component: HabitSpecificComponent
      },
      {
        path: 'basic',
        component: HabitBasicComponent
      },

    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HabitWizardPageRoutingModule { }
