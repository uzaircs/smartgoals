import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HabitPositiveTypicalComponent } from './habit-positive-typical.component';

describe('HabitPositiveTypicalComponent', () => {
  let component: HabitPositiveTypicalComponent;
  let fixture: ComponentFixture<HabitPositiveTypicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitPositiveTypicalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HabitPositiveTypicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
