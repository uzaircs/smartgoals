import { Component, OnInit } from '@angular/core';
import { PickerController, NavController, ToastController } from '@ionic/angular';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { HabitContainerService } from '../habit-container/habit-container.service';
import { RepeatItem, TimePeriod } from 'src/app/models/repeat/repeat-item';

@Component({
  selector: 'app-habit-positive-typical',
  templateUrl: './habit-positive-typical.component.html',
  styleUrls: ['./habit-positive-typical.component.scss'],
})
export class HabitPositiveTypicalComponent implements OnInit {
  dividers = ['Everyday', 'Twice a day',
    'Thrice a day',
    'Every week', 'Every Month', 'Once in 2 days', 'Once in 3 Days', 'Some days of week (Custom)', 'No of days per period (Custom)'
  ];
  $valid = {
    name: true
  };
  daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  customPeriod = false;
  periodType = 0;
  habit: HabitItem;
  constructor(private toast: ToastController, private nav: NavController, private container: HabitContainerService) { }
  stacking() {
    this.validate().then(done => {
      if (done) {
        this.container.setItem(this.habit);
        this.nav.navigateForward('habit-wizard/preview');
      } else {
        this.validationError();
      }
    }).catch(err => this.validationError());

  }
  async validationError() {
    const toast = await this.toast.create({
      header: 'Name is required',
      message: 'Please enter a habit name and try again',
      duration: 1500,
      color: 'danger'
    });
    toast.present();
  }
  clearValidations() {
    this.$valid.name = true;
  }
  validate(): Promise<boolean> {
    this.clearValidations();
    return new Promise((resolve, reject) => {
      if (!this.habit.name) {
        reject('Name is a required field');
        this.$valid.name = false;
      } else {
        this.$valid.name = true;
      }
      if (this.$valid.name) {
        resolve(true);
      }
    });

  }

  getNumbers() {
    const options = [];
    this.dividers.forEach((d, i) => {
      options.push({
        text: d,
        value: i
      });
    });
    return options;
  }
  ngOnInit() {
    this.habit = this.container.getItem();
  }

}
