import { Component, OnInit } from '@angular/core';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { HabitContainerService } from '../habit-container/habit-container.service';
import { NavController, ToastController } from '@ionic/angular';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-habit-basic',
  templateUrl: './habit-basic.component.html',
  styleUrls: ['./habit-basic.component.scss'],
})
export class HabitBasicComponent implements OnInit {
  habit: HabitItem;
  $valid = {
    name: true
  };
  constructor(
    private container: HabitContainerService,
    private navCtrl: NavController,
    private toast: ToastController
  ) { }
  async errorToast(message: string) {
    const toast = await this.toast.create({
      message,
      header: 'Please check',
      color: 'danger',
      duration: 2000
    });
    toast.present();
  }
  clearValidations() {
    this.$valid.name = true;
  }
  validate(): Promise<boolean> {
    this.clearValidations();
    return new Promise((resolve, reject) => {
      if (!this.habit.name) {
        reject('Name is a required field');
        this.$valid.name = false;
      } else {
        this.$valid.name = true;
      }
      if (this.$valid.name) {
        resolve(true);
      }
    });

  }
  preview() {

    this.validate().then(() => {
      this.container.setItem(this.habit);
      this.navCtrl.navigateForward('/habit-wizard/preview');
    }).catch(err => this.errorToast(err));

  }
  ngOnInit() {
    this.habit = this.container.getItem();
  }
  ionViewWillLeave() {



  }
}
