import { Component, OnInit, Type } from '@angular/core';
import { HabitItem, WinCondition, ConditionPolarity, Period } from 'src/app/models/habits/habit-item';
import { HabitContainerService } from '../habit-container/habit-container.service';
import { TimePeriod, RepeatItem } from 'src/app/models/repeat/repeat-item';
import { NavController, ToastController } from '@ionic/angular';
import { PeriodFormatter, TimePeriodItem } from '../habit-container/validator.service';

@Component({
  selector: 'app-habit-specific',
  templateUrl: './habit-specific.component.html',
  styleUrls: ['./habit-specific.component.scss'],
})
export class HabitSpecificComponent implements OnInit {
  customPeriod = false;
  habit: HabitItem;
  condition: WinCondition = new WinCondition();
  polarity: ConditionPolarity = ConditionPolarity.EXACTLY;
  periods = ['Day', 'Week', 'Month', 'Year'];
  $valid = {
    polarity: true,
    value: true,
    startDate: true,
    period: true,
    interval: true
  }
  selectedPeriod = {
    period: 'Day',
    interval: 1,
    $period: TimePeriod.DAY
  };
  constructor(
    private toast: ToastController,
    private container: HabitContainerService,
    private navCtrl: NavController
  ) { }
  async errorToast(message: string) {
    const toast = await this.toast.create({
      message,
      header: 'Please check',
      color: 'danger',
      duration: 2000
    });
    toast.present();
  }
  habitPage() {
    if (this.container.isUpdating) {
      this.navCtrl.pop();
    }
  }
  next() {
    this.condition.polarity = this.polarity;
    this.habit.repeatItem.interval = this.selectedPeriod.interval;
    this.habit.repeatItem.timePeriod = this.selectedPeriod.$period;
    this.habit.condition = this.condition;
    this.habit.repeatItem.repetitions = this.condition.value;
    this.container.setItem(this.habit);
    this.validate().then(() => {
      this.navCtrl.navigateForward('/habit-wizard/basic');
    }).catch((err) => this.errorToast(err));

  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.polarity) {
        reject('Please select the For condition for this habit');
        this.$valid.polarity = false;
      } else {
        this.$valid.polarity = true;
      }
      if (this.condition.value <= 0) {
        const t = new TimePeriodItem(this.habit);
        reject('Please select no of times this habit should be completed per ' + t.toReadableFormat());
        this.$valid.value = false;
      } else {
        this.$valid.value = true;
      }
      if (!this.selectedPeriod.period) {
        reject('Please select a valid period');
        this.$valid.period = false;
      } else {
        this.$valid.period = true;
      }
      if (!this.habit.startDate) {
        reject('Please enter a valid starting date for this goal');
        this.$valid.startDate = false;
      } else {
        this.$valid.startDate = true;
      }
      if (!this.selectedPeriod.period) {
        reject('Please select a valid period');
        this.$valid.period = false;
      } else {
        this.$valid.period = true;
      }
      if (this.$valid.interval && this.$valid.period && this.$valid.polarity
        && this.$valid.startDate && this.$valid.value) {
        resolve(true);
      }
    });

  }
  periodChanged() {
    this.selectedPeriod.$period = (this.periods.indexOf(this.selectedPeriod.period) + 1);
  }
  ngOnInit() {
    this.habit = this.container.getItem();
    if (this.container.isUpdating) {
      this.condition = this.habit.condition;
      this.polarity = this.habit.condition.polarity;

    } else {

      this.habit.repeatItem = new RepeatItem();
      this.habit.repeatItem.repetitions = 1;
      this.condition.value = 1;
    }


  }

}
