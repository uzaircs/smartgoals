import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HabitWizardPage } from './habit-wizard.page';

describe('HabitWizardPage', () => {
  let component: HabitWizardPage;
  let fixture: ComponentFixture<HabitWizardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitWizardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HabitWizardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
