import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HabitStackingComponent } from './habit-stacking.component';

describe('HabitStackingComponent', () => {
  let component: HabitStackingComponent;
  let fixture: ComponentFixture<HabitStackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitStackingComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HabitStackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
