import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { HabitContainerService } from '../habit-container/habit-container.service';
import { HabitItem } from 'src/app/models/habits/habit-item';

@Component({
  selector: 'app-habit-tracking',
  templateUrl: './habit-tracking.component.html',
  styleUrls: ['./habit-tracking.component.scss'],
})
export class HabitTrackingComponent implements OnInit {
  habit: HabitItem;
  positiveTypical() {
    this.validate().then(() => {
      this.container.setItem(this.habit);
      if (this.habit.method == 1) {
        this.navCtrl.navigateForward('/habit-wizard/typical');
      }
      if (this.habit.method == 2) {
        this.navCtrl.navigateForward('/habit-wizard/specific');
      }
    }).catch(() => this.errorToast());
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.habit.method) {
        reject();
      } else {
        resolve(true);
      }
    });

  }
  async errorToast() {
    const toast = await this.toast.create({
      message: 'Please select a tracking option',
      header: 'No option selected',
      color: 'danger',
      duration: 2000
    });
    toast.present();
  }
  constructor(
    private navCtrl: NavController,
    private container: HabitContainerService,
    private toast: ToastController
  ) { }

  ngOnInit() {
    this.habit = this.container.getItem();
  }

}
