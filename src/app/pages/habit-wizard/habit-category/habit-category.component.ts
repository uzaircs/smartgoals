import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { HabitItem } from 'src/app/models/habits/habit-item';
import { HabitContainerService } from '../habit-container/habit-container.service';

@Component({
  selector: 'app-habit-category',
  templateUrl: './habit-category.component.html',
  styleUrls: ['./habit-category.component.scss'],
})
export class HabitCategoryComponent implements OnInit {
  category: any = null;
  habit: HabitItem;
  constructor(
    private navCtrl: NavController,
    private container: HabitContainerService,
    private toast: ToastController
  ) { }
  async errorToast() {
    const toast = await this.toast.create({
      message: 'Please choose an option',
      header: 'Nothing Selected',
      color: 'danger',
      duration: 2000,
    });
    toast.present();
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.category) {
        reject();
      } else {
        resolve(true);
      }
    });
  }
  tracking() {
    this.validate().then(done => {
      if (done) {
        this.container.setItem(this.habit);
        this.navCtrl.navigateForward('/habit-wizard/tracking');
      } else {
        this.errorToast();
      }
    }).catch(() => this.errorToast());
  }
  ngOnInit() {
    this.habit = this.container.getItem();
  }

}
