import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HabitWizardPageRoutingModule } from './habit-wizard-routing.module';

import { HabitWizardPage } from './habit-wizard.page';
import { HabitTrackingComponent } from './habit-tracking/habit-tracking.component';
import { HabitCategoryComponent } from './habit-category/habit-category.component';
import { HabitPositiveTypicalComponent } from './habit-positive-typical/habit-positive-typical.component';
import { HabitSpecificComponent } from './habit-specific/habit-specific.component';
import { HabitBasicComponent } from './habit-basic/habit-basic.component';
import { PipesModule } from 'src/app/pipes/pipes/pipes.module';
import { HabitReviewComponent } from './habit-review/habit-review.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HabitWizardPageRoutingModule,
    PipesModule
  ],
  declarations: [
    HabitWizardPage,
    HabitCategoryComponent,
    HabitTrackingComponent,
    HabitPositiveTypicalComponent,
    HabitSpecificComponent,
    HabitBasicComponent,
    HabitReviewComponent
  ],
  exports: [
    HabitWizardPage,
    HabitCategoryComponent,
    HabitTrackingComponent,
    HabitPositiveTypicalComponent,
    HabitSpecificComponent,
    HabitBasicComponent,
    HabitReviewComponent
  ]

})
export class HabitWizardPageModule { }
