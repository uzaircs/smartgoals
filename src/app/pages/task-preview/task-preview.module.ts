import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskPreviewPageRoutingModule } from './task-preview-routing.module';

import { TaskPreviewPage } from './task-preview.page';
import { PipesModule } from 'src/app/pipes/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaskPreviewPageRoutingModule,
    PipesModule
  ],
  declarations: [TaskPreviewPage]
})
export class TaskPreviewPageModule { }
