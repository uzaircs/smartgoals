import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TaskPreviewPage } from './task-preview.page';

describe('TaskPreviewPage', () => {
  let component: TaskPreviewPage;
  let fixture: ComponentFixture<TaskPreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskPreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TaskPreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
