import { Component, OnInit } from '@angular/core';
import { Task, Goal } from 'src/app/service/database.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalStorageService } from 'src/app/service/global-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-task-preview',
  templateUrl: './task-preview.page.html',
  styleUrls: ['./task-preview.page.scss'],
})
export class TaskPreviewPage implements OnInit {
  task: Task;
  ready = false;
  subcription: Subscription;
  goal: Goal;

  constructor(
    private route: ActivatedRoute,
    private db: GlobalStorageService,
    private router: Router
  ) { }
  work() {
    this.router.navigate(['simple-count-down/task/', this.task.hash]);
  }
  deferredLoad(): Promise<boolean> {

    return new Promise((resolve, reject) => {
      this.route.params.subscribe(params => {

        const hash = params.hash;
        if (!hash) {
          reject('No Hash specified');
        } else {
          this.subcription = this.db.getTasks().subscribe(tasks => {
            const task = tasks.filter(t => t.hash === hash);
            if (task && task[0]) {

              this.task = task[0];
              this.db.getGoals().subscribe(goals => {
                const goal = goals.find(g => g.hash === task[0].goal);
                if (goal) {
                  this.goal = goal;
                  setTimeout(() => {
                    resolve(true);
                  }, 500);
                } else {
                  reject('Goal not found for this task');
                }
              });

            } else {
              reject('Task not found');
            }
          });
        }

      });
    });
  }
  ionViewDidLeave(): void {
    this.task = null;
    this.subcription.unsubscribe();

  }
  ngOnInit() {
    this.deferredLoad().then(done => {
      this.ready = true;
    })
  }

}
