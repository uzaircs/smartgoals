import { Component, OnInit } from '@angular/core';
import { Goal } from 'src/app/service/database.service';
import { Router } from '@angular/router';
import { DingSoundService } from 'src/app/utils/ding-sound.service';
import { GlobalStorageService } from 'src/app/service/global-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss', '../../header.scss'],
})
export class HomePage implements OnInit {
  ready = false;
  slideOpts = {
    /*  initialSlide: 1, */
    speed: 400,
    slidesPerView: 1.5,
    spaceBetween: 8,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  };
  goals: Goal[] = [];

  constructor(private router: Router, private ding: DingSoundService, private db: GlobalStorageService) {
    this.deferredLoad().then(ready => {
      this.ready = ready;
    });
  }
  deferredLoad(): Promise<boolean> {
    return new Promise(resolve => {
      this.db.getGoals().subscribe(goals => {
        this.goals = goals;
        setTimeout(() => {
          resolve(true);
        }, 1000);

      });
    });
  }
  viewGoal(goal: Goal) {

    this.router.navigate(['/goal-view/', goal.Id]);
  }
  ionViewDidEnter(): void {
    console.log('view entered');
  }
  ionViewDidLeave(): void {
    console.log('view unload');
  }
  ngOnInit() {

  }

}
