import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoalViewPageRoutingModule } from './goal-view-routing.module';

import { GoalViewPage } from './goal-view.page';
import { UIComponentsModule } from '../uicomponents/uicomponents.module';
import { PipesModule } from '../pipes/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UIComponentsModule,
    GoalViewPageRoutingModule,
    PipesModule
  ],
  declarations: [GoalViewPage]
})
export class GoalViewPageModule { }
