import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoalViewPage } from './goal-view.page';

describe('GoalViewPage', () => {
  let component: GoalViewPage;
  let fixture: ComponentFixture<GoalViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoalViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
