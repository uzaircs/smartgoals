import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GlobalStorageService } from '../service/global-storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Goal, Task } from '../service/database.service';
import { ToastController, ActionSheetController, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GoalStatistics } from '../interfaces/goal-statistics';
import { StatisticsService } from '../service/statistics.service';
import { GoalMenuPopoverComponent } from '../popovers/goal-menu-popover/goal-menu-popover.component';
import { SortPopoverComponent } from '../popovers/sort-popover/sort-popover.component';

@Component({
  selector: 'app-goal-view',
  templateUrl: './goal-view.page.html',
  styleUrls: ['./goal-view.page.scss', '../header.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class GoalViewPage implements OnInit {
  goal: Goal;
  subscription: Subscription;
  tasks: Task[];
  ready = false;
  goalStats: GoalStatistics;
  value: number;

  constructor(
    private db: GlobalStorageService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastController,
    private actionSheet: ActionSheetController,
    private statService: StatisticsService,
    private popover: PopoverController
  ) { }
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 1.2,
    centeredSlides: true,

  };
  async settingsPopover(ev: any) {
    const popover = await this.popover.create({
      component: GoalMenuPopoverComponent,
      mode: 'md',
      event: ev,
      translucent: true,
      componentProps: {
        goal: this.goal
      }
    });
    popover.present();
  }
  async sortPopover(ev: any) {
    const popover = await this.popover.create({
      component: SortPopoverComponent,
      mode: 'md',
      event: ev,
      translucent: true
    });
    popover.present();
  }

  addTask() {
    this.router.navigate(['/task-form/', this.goal.hash]);
  }
  async presentToast(): Promise<any> {
    const msg = await this.toast.create({
      message: 'Error opening this goal',
      translucent: true,
      color: 'danger',
      duration: 2000
    });
    msg.present();
  }

  deferredLoad(): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {

        this.route.params.subscribe(p => {
          const hash = p.goal;

          this.db.getGoal(hash).then(goal => {
            this.goal = goal;
            this.subscription = this.db.getTasks().subscribe(tasks => {
              this.tasks = tasks.filter(task => task.goal === this.goal.hash);
              resolve(true);
            });
            this.statService.goalStats(this.goal).then(done => {
              this.goalStats = done;
              this.value = this.goalStats.completedPercent;
            });

          }).catch(err => {
            this.presentToast();
            this.router.navigateByUrl('');
          });
        });
      }, 50);
    });
  }
  ngOnInit() {
    this.deferredLoad().then(ready => {
      this.ready = ready;
    });
  }
  async presentActionSheet() {
    const sheet = await this.actionSheet.create({
      header: 'What do you want to add?',
      buttons: [
        {
          text: 'Task',
          icon: 'document-text-outline',

          handler: () => {
            this.addTask();
          }
        },
        {
          text: 'Sub Goal',
          icon: 'flag-outline'
        },
        {
          text: 'Work Plan',
          icon: 'folder-outline'
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    sheet.present();
  }
  ionViewWillLeave(): void {
    this.subscription.unsubscribe();
    this.tasks = [];
    this.ready = false;
    this.goal = null;
  }
  ionViewDidEnter(): void {
    this.ready = false;
    this.deferredLoad().then(done => {
      this.ready = done;
    });
  }
  /* closeSubscription() {
    subscription.unsubscribe();
  } */
}
