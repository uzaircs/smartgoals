import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, ToastController, IonContent } from '@ionic/angular';
import { ModalPageComponent } from '../utils/modal-page/modal-page.component';
import { Task, TaskModel, Goal } from '../service/database.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalStorageService } from '../service/global-storage.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.page.html',
  styleUrls: ['./task-form.page.scss', '../header.scss'],
})
export class TaskFormPage implements OnInit {
  @ViewChild('taskFormContent') taskFormContent: IonContent;
  repeat = {
    selected: ''
  };
  dayCheck = false;
  task: Task = new TaskModel();
  port: any;
  goal: Goal;
  $task = {
    name: true
  }
  validate(): Promise<boolean> {
    return new Promise((resolve, reject) => {

      if (!this.task.name) {
        this.$task.name = false;
        reject('Task name is required field');
      } else {
        this.$task.name = true;
      }
      if (this.$task.name) {
        resolve(true);
      }
    });

  }
  constructor(
    private db: GlobalStorageService,
    private router: Router,
    private toast: ToastController,
    public modalController: ModalController,
    private route: ActivatedRoute) {
    route.params.subscribe(p => {
      const goal = p.goal;
      this.task.goal = goal;
      this.db.getGoal(goal).then(g => {
        this.goal = g;
      });
    });

  }
  async errorToast(message: string) {
    const toast = await this.toast.create({
      message,
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
  async presentToast() {
    const toast = await this.toast.create({
      message: 'Task added',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }
  addToDay(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      resolve(true);
    });

  }
  addTask() {
    this.validate().then(done => {
      if (done) {
        this.db.addTask(this.task).then(d => {
          this.presentToast();
          this.router.navigate(['/goal-view/', this.task.goal]);

        });
      }
    }).catch(err => {
      this.errorToast(err);
      this.taskFormContent.scrollToPoint(0, 0, 300);
    });
  }
  save() {
    if (this.dayCheck) {
      this.addToDay().then(done => {
        if (done) {
          this.addTask();
        }
      });
    } else {
      this.addTask();
    }

  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPageComponent
    });
    return await modal.present();
  }
  repeatChanged() {
    console.log(this.repeat.selected);
    if (this.repeat.selected === 'c') {
      this.presentModal();
    }

  }
  ngOnInit() {
  }

}

