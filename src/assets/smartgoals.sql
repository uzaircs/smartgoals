
DROP TABLE IF EXISTS goal;
CREATE TABLE goal (Id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, name VARCHAR (1024) NOT NULL, pre_req INTEGER, repeat_type INTEGER, start_date DATETIME, end_date DATETIME, status INT, created_at DATETIME, updated_at DATETIME, priority INT);

-- Table: tasks
DROP TABLE IF EXISTS tasks;
CREATE TABLE tasks (Id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR (1024) NOT NULL, goal INTEGER REFERENCES goal (Id) ON DELETE CASCADE, repeat_type INTEGER NOT NULL DEFAULT (0), repeat_every INTEGER, priority INTEGER NOT NULL DEFAULT (1), reminder DATETIME, estimated_time BIGINT, milestone INTEGER, notes TEXT);

