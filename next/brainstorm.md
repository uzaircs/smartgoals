How to calcuate habit streak?
==

`HabitItem`

`CheckResultItems[] =`

```typescript
{
    date: Date;
    verified = false;
    result: CheckResult;
    requiredValue: number;
    storedValue: number;
}
```

`GetStreakType` will verify streak in **3** `HabitResults`

1. Pass habit to a method
2. Get streaks as array
    
```typescript
let streak = getStreak(habit);
```
> Expected output

`[2,4,5,3,5]`

> We already have

`GetStreakType(previous,current,next)`

*Where previous, current and next are `CheckResult` items*

> Streak also has more properties e.g

1. StartDate
2. EndDate
3. StreakValue

```typescript
function getStreak(habit : HabitItem) : StreakItem[]{
    let result = getResult();
    let streaks = [];
    result.forEach((item,index,array)=>{
        if(array[i + 1] || array[i - 1]){
            let prev = array[i - 1];
            let current = item;
            let next = array[i + 1];
            let streakType = GetStreakType(prev,current,next);
            let hasStreak = streakType !==NONE;
            let streakItem = new StreakItem();
            streakItem.value = 0;
            if(hasStreak){
                
                streakItem.startDate = prev.date;
                while(hasStreak){
                streakItem.value++;
                streakItem.endDate = current.date;

                
                streakType = GetStreakType(prev,current,next);
                hasStreak = streakType !==NONE;
                }
            }
         
            streaks.push(streakItem)
        }
    })
}
```

> Result item = `[1/1/2020:verified,1/2/2020:verified,1/3/2020:vefiried,1/4/2020:verified, 1/5/2020:unvefieid]`

> When iterator on 0    

```typescript
prev = null;
current = true;
next = true;
streakType != NONE = true;

THEN
streakItem.endDate = 1/4/2020?

WHEN on NEXT
prev = true;
current = true;
next = false;

THEN
1. streamItem.endDate = current.date = 1/4/2020?

WHEN ON 1/5/2020
prev = true
current = false;
next = null;
while condition should be current = verified

WHEN ON (date where current = true and prev = false and next = false eg 1/7/2020)
prev = false // iterator will not reach here
current = true;
next = false;
if(current ! verified){
    //streak broken
    //break while loop
}
```
> dry run
```typescript
while(current!=verified){
    next = current + 1
    prev = current - 1
    getStreak (prev,current,next)
    if(hasStreak){
        streakItem.endDate = current.date;
        streakItem.value++;
    }
    current = x;
}
push(streakItem)
```

> *Dry Run 3*
```typescript

for(i=0;i < results.length;i++){
    
    const current = results[i];
    const next = results[i +1];
    const prev = results[i - 1];
    const streak = getStreak(prev,current,next)
    if(streak){
        const item = new StreakItem();
        
    }
}
```

How to find streak end when we have starting
----
```typescript
let streak = getStreak();
//we have current = verified and streak = true;
// get next false current index 
// first index after i that contains false verified = x
// x=  i + arr.slice(i).findIndex(item => !item.verified)
// x = StreakEndIndex
// streakItem.endDate = item[streakEndIndex].date;
```