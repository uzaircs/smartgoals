# SMART Goals Development Lifecycle
### should break down to smaller phases of iterations
## Version 0.1.0 - `Apple`
> Expected End Date 16th Mar
1. Performance overview can be delayed [`Done`]
2. Stopwatch and countdown is important [`DONE`] [Carried] [v2Required]
3. UI/UX improvements can be delayed  [`Done`]
4. Main page header simplification ?    [`Done`]
    + Show active goals [`Done`]
    + Show active tasks  [`Done`]
    + Show active habits  [`Done`]
    + Show Required Work Rate [Later]
    + Show Current Work Rate [Later]

## Version 0.1.1 [`ApplePie`] : [`DONE`]
> Expected End Date : 19th Mar
1. Goal Delete Option [`Done`]
2. CRUD Tasks [`DONE`]
3. CRUD Habits [`PARTIAL`]
4. Goal Progress card [DONE]
5. Goal Edit Verification [`DONE`]
6. Goal Form Validations [`DONE`]
7. Task Form Validations [`DONE`]
8. Habit Form validations [`DONE`]


## Version 0.2 - `AppleCake` [`IN PROGRESS`]
> Expected end date : 21st Mar
1. Habit View Page  
    + Calendar Display [`Done`]
    + `onItemClick` - Show Breakdown by Day     [`Done`]
        + `GetBreakDown(date)` [`Done`]
        + Breakdown model [`Done`]
        + bind model to HTML [`Done`]
        + Numbers and date formatting [`Done`]
2. Habit Edit Form
    + Edit Habit Name ['Done']  
    + Edit habit repeation settings = `ResetHistoryItems` ['Done']
    + Form Validations [`Done`]
    + Change start date [`Done`]
    + Change period [`Done`]
    + Old wizard can be used [`Done`]

## Version 0.3 - `BananaShake`:
> Expected end Date **Wednesday 25th Mar**
1. Habit Wizard Remake
    + Quit habits logic - <span style="color:#ee84d8"> **InspirationRequired**</span> [Removed]
    + Simple done/fail testing *fix* [Progress]
    + Wizard validations - *add* [Done]
    + Wizard toast messages - *add* [Done]
    + Wizard default values *add* [Done]
    + Wizard Threshold - *fix* [Removed]

## Version 0.3.1 - `BananaCustard` : 
> Expected End Date **27th March**
1. Goal View Header - Add More information and make consistent
    + Goal statistics - `?`
    + Goal progress
    + Goal Dates
2. Task Sorting
    + Seperate UI Strip for sort menu
    + UI Strip can also have goal settings
    + 

## Version 0.4 - `CaramelMeta`
1. Goal version mantainence  
    + Log every goal changes and find out when to change versions
    + Track progress for different versions
    + Version history page

## Version 0.4 - `CaramelCream`
> **1st April 2020**
1. Subgoals
    + Subgoals version tracking - *minor versions* + *increment*
    + Sub goal workflow planning
    + Sub goal services and classes
        + Task entries
        + Update done        
    + Sub goal views
        + Sub goal card view
        + Sub goal check remap to goal *`parent/child`* to `GoalView`

2. Timers
    + Attach timer to tasks
    + Timer services and storages
    + Timer verify mappings 
    + Pomodoro logic check
        + Pomodoro `services/classes`
    + Timer View





    