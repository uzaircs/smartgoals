```typescript
interface IPomodoro{
    elapsed : TimeElapsed;
    paused = false;
    required : TimeElapsed;
    start() : Observable<TimeElapsed>
    pause() : Promise<boolean>
    stop() : Promise<TimeElapsed>
    constructor(hash : string)
}

class Pomodoro extends AutoUnique,implements IPomodoro{
    elapsed : TimeElapsed;
    paused = false;
    required : TimeElapsed;
    start(){
        interval.subscribe(tick)
    }
    private onTick(){
        if(!paused){
            elapsed.tick();
        }
    }
    stop(){
        interval.unsubscribe()
    }
    toggle(){
        this.paused = !this.paused;
    }
    constructor(required : TimeElapsed){}
}
```