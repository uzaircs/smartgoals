# Pomodoro 1.0
## Pomodoro Description
There are six steps in the original technique:

Decide - `[var - select input]` on the task to be done.
Set the pomodoro timer (traditionally to **25 minutes** `[var - select input]`
Work `[var- start event]` on the task.
End work when the timer rings `[var ring event]` and put a checkmark `[event done]` on a piece of paper.[6]
If you have fewer than four `[-var condition]` checkmarks, take a short break (3–5 minutes), then go to step 2.
*After four pomodoros* `[var 4= count]`, `[count > 4 =]` take a longer break (15–30 minutes), reset your checkmark count to zero, then go to step 1.
### Basic Scope
1. Stopwatch should track time for a task `Done`
2. It should have a default time `Done`
    + Default time get from estimated time?
3. It should have intervals `Next`
    + Pomodoro `services/classes`
4. It should have time for break `Next`
5. User input can be set time, set break time, set intervals `Next`
6. Default time can be `pomodoroDefaultTime`, while default breaktime can be around the same, 
7. All time should be logged with unique hash that can be mapped to tasks and joined with goal later on. `Done - partial`


### Timer View
1. View should be simple
2. Workflow should be flexible
    + Constructor
        + Task (Preset from when coming from task) `prop`
        + Pomodoro timer time - default 25 Minutes -> `prop`
    + Start Logic `Done`
        + Start Timer -> `Method` -> `Observable` -> `Type` -> `Observable<TimeElapsed>` -> 
        + Time Converter -> `Method` -> returns `TimeElapsed` ->
            + `TimeElapsed = seconds,minutes,hours` 
    + Stop `Done`
        + Subscription Stop? interval service rxjs stop or cancel `call`
    + Pause `Done`
        + `isPaused` -> `no next()`
    + Ability to change inputs - Resets clock but logs it
        + Ask user that changing will abandon and `reset/remove`
        + Give user the option to save current progress when changing
        