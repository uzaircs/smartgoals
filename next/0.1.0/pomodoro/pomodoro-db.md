## How to track timers?

1. Timers should be stored in db
2. TimeElapsed can be stored? or whole timer?
3. TimeElapsed should be stored in db as object
4. dbStore can contain TaskTimers log
    + Each log entry will be TaskPomodoro? `Yes`
    + Get Task Timers = Method -> returns all pomodoro
    + When should it be saved?
        + onStop **`save`**
    + When the timer is stopped is should be saved automatically
    + Running timer should be held in a service
    + This service will handle wether timer should be saved or not
    + `onStop` -> `TimerService` -> `save()` -> saves timer
    + `TimerService` -> `get()` -> returns running timer
    + More than 1 timer can not run
    + `TimerService` -> `hasTimer`
    + `TimerPage` -> `isSuccess` -> `markTimerSuccess`
    + `TimerPage` -> `DialogRequired` -> `SaveProgress`, `DiscardProgress`
    + `onSaveProgress` -> *Mark timer as passed*
    + `Pass` = when timer = succesful progress on a task/goal/habit
        
        

```
Timer view will have pause and stop button, pause will just toggle the state 
when the timer is stopped it can be due to user stopped it or time has ended, when the time has ended it should be saved, if the user stopped it then he should be asked wether he wants to save progress of this timer.

If yes then progress will be saved.

He can also continue from where he left

For this he should see a list of all IPomodoro Timers
    1. Sorted by date ended
    2. This list will show incomplete timers as well,
    3. User can click on <any||incomplete>? timer to start from where he left
        + Any or incomplete?
        + If any then completed timer will show up in TimerView he cannot start it since it is already completed
    4. Maybe give an option to clone timer from list? an option to redo? Rework = Restart
    5. If progress left then restart ask user to restart from where he left
        else start new instance.
```