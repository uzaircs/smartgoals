Required fields in habit form 

1. Track required
2. Default tracker values
    + For (Required)
    + No times (required)
    + Starting from (required)

3. Advanced settings
    + Period type required
    + Period value required and number
    + End on valid date
4. Habit name required
5. Repetitions?