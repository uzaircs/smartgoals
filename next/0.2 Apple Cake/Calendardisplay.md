0.2 Apple Cake Day 1
--
1. Habit View Page  
    + Calendar Display [`Done`]
    + `onItemClick` - Show Breakdown by Day  [`In Progress`]

How to show breakdown by day?

    When user clicks a calendar date they should be able to view
    breakdown for that day in a card?

> What Will be displayed in breakdown?

1. Number of entries for that day
2. Was it a success?
    + If not, why?
3. What was the streak that day?
4. Number of days spent on this date.
5. Total Number of times completed till this day
6. Total number of times failed till this day



