Breakdown Model
--
what sould a breakdown model contain?  

When user clicks on breakdown item we have `Habit` and `Date`

`GetBreakdown(habit,date)`
should `return` 
+ Entries for this date
+ Habit TimePeriodItem [Fixed]
+ if timePeriod is not day then what range does it fall in?
    + Get Range as `[date0,date1]`
        + Where `date0` is selectedPeriod Starting Boundary & `date1` is selectedPeriod ending boundary
            + For example if user clicks 13/1/2020 -> MedianKey = 24/1/2020 = `date1` - interval * TimePeriod = `date0`
            + `date1` - interval = `date0`
            + if null ? resultsPadding?
        + This period will be set as string
        + Range will contain the entries for this range resulting effecting `TotalEntries`
        + Indicator will show the status of this range.
    + 
