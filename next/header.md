# Header Structure

What should the header be like?
-
i*t should display vital information*

vital information from 
1. goals 
2. tasks 
3. habits
## basic structure


## KPIs

`Productivity Rate` -> **How is this defined?**

    Productivity rate = Current Rate / Required Rate

Best performing habit : Some habit  
Worst performing habit : somehabit  
Highest streak : n - some habit  
Overall habits health - score/percent  
Breakdown  

Every **`Habithealth`**
Reason for `health damage`  
`Health damages = missing details, incomplete performance`

1. `Goal`  
    + ETA : n(hours)  - **`TimeRemaining`** 
    + Estimated spent  - **`TimeSpent`**
    + Current Work Rate  - `WorkRate`
    + Milestones acheived - `AcheivedMilestones`
    + Required Work Rate  - `RequiredWorkRate`
    + Overal goal health  - `GoalHealth`
    ---
    >Remaining stuff for `Goal`
    + Goal Time ?  
    + 3 tasks / day
    + 7 Tasks / day  
    + ?
    

2. Tasks :  
    + Completed Total - `TotalCompletedTasks`
    + Remaining Total - `RemainingTasks`
    + Overdue : Total - `TotalOverdueTasks`
    + Task Time - `EstimatedTimeRemaining`
    + Health - `OverallTaskScore` **?**
    + Health Meter Measurement same as habit and goal

